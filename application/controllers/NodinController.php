<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NodinController extends CI_Controller {

	var $API_Stylo2 ="";
	var $API_Stylo3 ="";
	var $API_Stylo4 ="";
	var $API_Nodin ="";
	var $API_NoNodin = "";
	var $API_Workflow= "";
	var $API_GetNo = "";
	var $API_Check = "";
	var $API_CreateNodin = "";
	var $API_CRQ = "";
	var $API_AutoNEID = "";
	var $API_Export = "";
	var $API_ExportNodin = "";

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->API_Stylo2="http://10.54.36.49/api-swap/Stylo2GController/";
		$this->API_Stylo3="http://10.54.36.49/api-swap/Stylo3GController/";
		$this->API_Stylo4="http://10.54.36.49/api-swap/Stylo4GController/";
		$this->API_Nodin="http://10.54.36.49/api-swap/NodinController/";
		$this->API_NoNodin="http://10.54.36.49/api-swap/NoNodinController/";
		$this->API_Count="http://10.54.36.49/api-swap/CountController/";
		$this->API_Workflow="http://10.54.36.49/apis-workflow/ManagerController";
		$this->API_GetNo="http://10.54.36.49/api-swap/GetNoController";
		$this->API_Check="http://10.54.36.49/api-swap/CheckController";
		$this->API_CreateNodin="http://10.54.36.49/api-swap/CreateNodinController";
		$this->API_Export="http://10.54.36.49/api-swap/ExportController";
		$this->API_AutoNEID = "http://10.54.36.49/api-swap/AutoNeidController";
		$this->API_CRQ = "http://10.54.36.49/api-swap/CRQController";
		$this->API_ExportNodin = "http://10.54.36.49/api-swap/ExportNodinController";

		session_start();
		// $this->load->library('session');
		$this->load->library('pagination');
		$this->load->library('curl');
		$this->load->library('excel');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->library("Nusoap_library", "");

		//untuk upload file ke remedy
		$config['upload_path']          = './file_upload/';
		$config['allowed_types']        = 'txt|xls|xlsx';
		$config['max_size']             = 1000;
		$this->load->library('upload', $config);
	}

	public function index(){
		if(isset($_GET['band'])) {
			if($_GET['band'] == '2G') {
				$jumlah_data = json_decode($this->curl->simple_get($this->API_Count.'?tabel=c_stylo_master_cell_2g'));

				$config = array();
				$config['query_string_segment'] = '?band=3G';
				$config['reuse_query_string'] = TRUE;
				$config["base_url"] = base_url() . "index.php/NodinController/index";
				$total_row = $jumlah_data;
				$config["total_rows"] = ceil($total_row/100);
				$config["per_page"] = 100;
				$config['first_link'] = FALSE;
				$config['last_link'] = FALSE;
				$config['use_page_numbers'] = TRUE;
				$config['display_pages'] = TRUE;
				$config['next_link'] = ' Next ';
				$config['prev_link'] = ' Previous ';
				$config['num_tag_open'] = ' <div style="display: none;"> ';
				$config['num_tag_close'] = ' </div>';
				$config['cur_tag_open'] = '&nbsp;<a class="current btn btn-primary">';
				$config['cur_tag_close'] = ' </a> '.' <a class="btn btn-default"> of '.$config['total_rows'].' </a> ';

				$this->pagination->initialize($config);
				if($this->uri->segment(3)){
					$next_page = $this->uri->segment(3);
					$page = ($next_page - 1) * 100;
				}
				else{
					$page = 0;
				}

				$str_links = $this->pagination->create_links();
				$data["links"] = explode('&nbsp;',$str_links );

				$data['datastylo'] = json_decode($this->curl->simple_get($this->API_Stylo2.'?limit='.$config['per_page'].'&page='.$page.'&cell_name='.$this->input->post('search')));
				$data['datanodin'] = json_decode($this->curl->simple_get($this->API_CreateNodin.'?limit='.$config['per_page'].'&page='.$page.'&swoid='.$this->input->post('search')));
				//if(isset($_GET['token'])) {
				$this->load->view('ListStylo',$data);
				//}else{
				//		header("location:http://10.54.36.49/landingPage");
				//}
			} elseif($_GET['band'] == '3G'){
				$jumlah_data = json_decode($this->curl->simple_get($this->API_Count.'?tabel=c_stylo_master_cell_3g'));

				$config = array();
				$config['query_string_segment'] = '?band=3G';
				$config['reuse_query_string'] = TRUE;
				$config["base_url"] = base_url()."index.php/NodinController/index";
				$total_row = $jumlah_data;
				$config["total_rows"] = ceil($total_row/100);
				$config["per_page"] = 100;
				$config['first_link'] = FALSE;
				$config['last_link'] = FALSE;
				$config['use_page_numbers'] = TRUE;
				$config['display_pages'] = TRUE;
				$config['next_link'] = ' Next ';
				$config['prev_link'] = ' Previous ';
				$config['num_tag_open'] = ' <div style="display: none;"> ';
				$config['num_tag_close'] = ' </div>';
				$config['cur_tag_open'] = '&nbsp;<a class="current btn btn-primary">';
				$config['cur_tag_close'] = ' </a> '.' <a class="btn btn-default"> of '.$config['total_rows'].' </a> ';

				$this->pagination->initialize($config);
				if($this->uri->segment(3)){
					$next_page = $this->uri->segment(3);
					$page = ($next_page - 1) * 100;
				}
				else{
					$page = 0;
				}

				$str_links = $this->pagination->create_links();
				$data["links"] = explode('&nbsp;',$str_links );

				$data['datastylo'] = json_decode($this->curl->simple_get($this->API_Stylo3.'?limit='.$config['per_page'].'&page='.$page.'&cell_name='.$this->input->post('search')));
				$data['datanodin'] = json_decode($this->curl->simple_get($this->API_CreateNodin.'?limit='.$config['per_page'].'&page='.$page.'&swoid='.$this->input->post('search')));
				//if(isset($_GET['token'])) {
				$this->load->view('ListStylo',$data);
				//}else{
				//		header("location:http://10.54.36.49/landingPage");
				//}
			} elseif($_GET['band'] == '4G') {
				$jumlah_data = json_decode($this->curl->simple_get($this->API_Count.'?tabel=c_stylo_master_cell_4g'));

				$config = array();
				$config['query_string_segment'] = '?band=3G';
				$config['reuse_query_string'] = TRUE;
				$config["base_url"] = base_url() . "index.php/NodinController/index";
				$total_row = $jumlah_data;
				$config["total_rows"] = ceil($total_row/100);
				$config["per_page"] = 100;
				$config['first_link'] = FALSE;
				$config['last_link'] = FALSE;
				$config['use_page_numbers'] = TRUE;
				$config['display_pages'] = TRUE;
				$config['next_link'] = ' Next ';
				$config['prev_link'] = ' Previous ';
				$config['num_tag_open'] = ' <div style="display: none;"> ';
				$config['num_tag_close'] = ' </div>';
				$config['cur_tag_open'] = '&nbsp;<a class="current btn btn-primary">';
				$config['cur_tag_close'] = ' </a> '.' <a class="btn btn-default"> of '.$config['total_rows'].' </a> ';

				$this->pagination->initialize($config);
				if($this->uri->segment(3)){
					$next_page = $this->uri->segment(3);
					$page = ($next_page - 1) * 100;
				}
				else{
					$page = 0;
				}

				$str_links = $this->pagination->create_links();
				$data["links"] = explode('&nbsp;',$str_links );

				$data['datastylo'] = json_decode($this->curl->simple_get($this->API_Stylo4.'?limit='.$config['per_page'].'&page='.$page.'&cell_name='.$this->input->post('search')));
				$data['datanodin'] = json_decode($this->curl->simple_get($this->API_CreateNodin.'?limit='.$config['per_page'].'&page='.$page.'&swoid='.$this->input->post('search')));
				//if(isset($_GET['token'])) {
				$this->load->view('ListStylo',$data);
				//}else{
				//		header("location:http://10.54.36.49/landingPage");
				//}
			}
		} else{
			$jumlah_data = json_decode($this->curl->simple_get($this->API_Count.'?tabel=c_stylo_master_cell_2g'));

			$config = array();
			$config["base_url"] = base_url() . "index.php/NodinController/index";
			$total_row = $jumlah_data;
			$config["total_rows"] = ceil($total_row/100);
			$config["per_page"] = 100;
			$config['first_link'] = FALSE;
			$config['last_link'] = FALSE;
			$config['use_page_numbers'] = TRUE;
			$config['display_pages'] = TRUE;
			$config['next_link'] = ' Next ';
			$config['prev_link'] = ' Previous ';
			$config['num_tag_open'] = ' <div style="display: none;"> ';
			$config['num_tag_close'] = ' </div>';
			$config['cur_tag_open'] = '&nbsp;<a class="current btn btn-primary">';
			$config['cur_tag_close'] = ' </a> '.' <a class="btn btn-default"> of '.$config['total_rows'].' </a> ';

			$this->pagination->initialize($config);
			if($this->uri->segment(3)){
				$next_page = $this->uri->segment(3);
				$page = ($next_page - 1) * 100;
			}
			else{
				$page = 0;
			}

			$str_links = $this->pagination->create_links();
			$data["links"] = explode('&nbsp;',$str_links );

			$data['datastylo'] = json_decode($this->curl->simple_get($this->API_Stylo2.'?limit='.$config['per_page'].'&page='.$page.'&cell_name='.$this->input->post('search')));
			$data['datanodin'] = json_decode($this->curl->simple_get($this->API_CreateNodin.'?limit='.$config['per_page'].'&page='.$page.'&swoid='.$this->input->post('search')));

			//if(isset($_GET['token'])) {
			$this->load->view('ListStylo',$data);
			//}else{
			//		header("location:http://10.54.36.49/landingPage");
			//}
		}
	}

	public function index_nodin(){
		$jumlah_data = json_decode($this->curl->simple_get($this->API_Count.'?tabel=t_nodin_stylo'));
		$config = array();
		$config["base_url"] = base_url() . "index.php/NodinController/index_nodin";
		$total_row = $jumlah_data;
		$config["total_rows"] = $total_row;
		$config["per_page"] = 100;
		$config['use_page_numbers'] = TRUE;
		$config['num_links'] = $total_row;
		$config['cur_tag_open'] = '&nbsp;<a class="current btn btn-primary">';
		$config['cur_tag_close'] = '</a>';
		// By clicking on performing NEXT pagination.
		$config['next_link'] = ' Next ';

		// By clicking on performing PREVIOUS pagination.
		$config['prev_link'] = ' Previous ';

		$this->pagination->initialize($config);
		if($this->uri->segment(3)){
			$page = ($this->uri->segment(3)) ;
		}
		else{
			$page = 0;
		}

		$str_links = $this->pagination->create_links();
		$data["links"] = explode('&nbsp;',$str_links );
		$data['datanodin'] = json_decode($this->curl->simple_get($this->API_Nodin.'?limit='.$config['per_page'].'&page='.$page.'&nodim_id='.$this->input->post('search')));

		//if(isset($_GET['token'])) {
		$this->load->view('UpdateNodin',$data);
		//}else {
		//    header("location:http://10.54.36.49/landingPage");
		//}
	}

	public function index_listnodin(){
		$jumlah_data = json_decode($this->curl->simple_get($this->API_Count.'?tabel=t_nodin_stylo'));
		$config = array();
		$config["base_url"] = base_url() . "index.php/NodinController/index_listnodin";
		$total_row = $jumlah_data;
		$config["total_rows"] = $total_row;
		$config["per_page"] = 100;
		$config['use_page_numbers'] = TRUE;
		$config['num_links'] = $total_row;
		$config['cur_tag_open'] = '&nbsp;<a class="current btn btn-primary">';
		$config['cur_tag_close'] = '</a>';
		// By clicking on performing NEXT pagination.
		$config['next_link'] = ' Next ';

		// By clicking on performing PREVIOUS pagination.
		$config['prev_link'] = ' Previous ';

		$this->pagination->initialize($config);
		if($this->uri->segment(3)){
			$page = ($this->uri->segment(3)) ;
		}
		else{
			$page = 0;
		}

		$str_links = $this->pagination->create_links();
		$data["links"] = explode('&nbsp;',$str_links );
		$data['crqs'] = json_decode($this->curl->simple_get($this->API_CRQ.'/CRQController'));
		$data['datanodin'] = json_decode($this->curl->simple_get($this->API_NoNodin.'?limit='.$config['per_page'].'&page='.$page.'&nodim_id='.$this->input->post('search')));

		//if(isset($_GET['token'])) {
		$this->load->view('ListNodin',$data);
		//}else {
		//    header("location:http://10.54.36.49/landingPage");
		//}
	}

	public function index_export(){
		$data['datanodin'] = json_decode($this->curl->simple_get($this->API_NoNodin."?nodin_id=".$_GET['nodin_id']));

		//if(isset($_GET['token'])) {
		$this->load->view('ExportNodin',$data);
		//}else {
		//    header("location:http://10.54.36.49/landingPage");
		//}
	}

	public function export_data(){
		$nodin_id = $this->input->post('nodin_id');
		$data = array(
			'NODIN_ID'  => $this->input->post('nodin_id'),
			'KEPADA'    => $this->input->post('kepada'),
			'DARI'      => $this->input->post('dari'),
			'LAMPIRAN'  => $this->input->post('lampiran'),
			'PERIHAL'   => $this->input->post('perihal'),
			'KONTEN'	=> $this->input->post('konten')
		);

		$insert =  $this->curl->simple_post($this->API_NoNodin.'/NoNodinController', $data, array(CURLOPT_BUFFERSIZE => 10));
		$datas['konten'] = json_decode($this->curl->simple_get($this->API_ExportNodin.'?nodin_id='.$nodin_id));
		$datas['datanodin'] = json_decode($this->curl->simple_get($this->API_Nodin.'/NodinController'));
		if($insert){
			// $this->session->set_flashdata('hasil','Insert Data Success');
		} else{
			// $this->session->set_flashdata('hasil','Insert Data Fail');
		}

		$this->load->view('laporanpdf',$datas);
	}

	public function export_data_excel(){
		$datanodin = json_decode($this->curl->simple_get($this->API_Export."?ne_id=".$_POST['filter_export']));
		$object = new PHPExcel();

		$object->setActiveSheetIndex(0);

		$table_columns = array(
			"Cell Name","Vendor","Regional","LAC/TAC","CI","New LAC/TAC","New CI","Site ID","NE ID","Band","Description Of Change",
			"Reason for Change","Executor PIC Company","Executor PIC Name","Executor PIC Phone","Executor PIC Email","NetworlElement Name",
			"User Access Login","NE Type","Area NE","Additional Information","Access Level","Rolekeys","Responsible Approval",
			"Detailed Location","NE Name (Impacted) List","NE Impact Description","NE Outage Duration (min)","Affected Service Description",
			"Sevice Outage Duration (min)","NE ID (Primary)","NE Name (Primary)","BTS Name","Frequency","Affected Service","Provinsi",
			"Kelurahan","Kecamatan","Cluster","City"
		);

		$column = 0;

		foreach($table_columns as $field)
		{
		 $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
		 $column++;
		}

		$excel_row = 2;

		foreach($datanodin as $nodin){
			 $object->getActiveSheet()->setCellValueByColumnAndRow(0,$excel_row,$nodin->CELL_NAME);
			 $object->getActiveSheet()->setCellValueByColumnAndRow(1,$excel_row,$nodin->VENDOR);
			 $object->getActiveSheet()->setCellValueByColumnAndRow(2,$excel_row,$nodin->REGIONAL);
			 if($nodin->BAND != '4G') {
				 $object->getActiveSheet()->setCellValueByColumnAndRow(3,$excel_row,$nodin->LAC);
			 }else{
				 $object->getActiveSheet()->setCellValueByColumnAndRow(3,$excel_row,$nodin->TAC);
			 }
			 $object->getActiveSheet()->setCellValueByColumnAndRow(4,$excel_row,$nodin->CI);
			 if($nodin->BAND != '4G') {
				 $object->getActiveSheet()->setCellValueByColumnAndRow(5,$excel_row,$nodin->NEW_LAC);
			 }else{
				 $object->getActiveSheet()->setCellValueByColumnAndRow(5,$excel_row,$nodin->NEW_TAC);
			 }
			 $object->getActiveSheet()->setCellValueByColumnAndRow(6,$excel_row,$nodin->NEW_CI);
			 $object->getActiveSheet()->setCellValueByColumnAndRow(7,$excel_row,$nodin->SITE_ID);
			 $object->getActiveSheet()->setCellValueByColumnAndRow(8,$excel_row,$nodin->NE_ID);
			 $object->getActiveSheet()->setCellValueByColumnAndRow(9,$excel_row,$nodin->BAND);
			 $object->getActiveSheet()->setCellValueByColumnAndRow(10,$excel_row,$nodin->DESCRIPTION_OF_CHANGE);
			 $object->getActiveSheet()->setCellValueByColumnAndRow(11,$excel_row,$nodin->REASON_FOR_CHANGE);
			 $object->getActiveSheet()->setCellValueByColumnAndRow(12,$excel_row,$nodin->EXECUTOR_PIC_COMPANY);
			 $object->getActiveSheet()->setCellValueByColumnAndRow(13,$excel_row,$nodin->EXECUTOR_PIC_NAME);
			 $object->getActiveSheet()->setCellValueByColumnAndRow(14,$excel_row,$nodin->EXECUTOR_PIC_PHONE);
			 $object->getActiveSheet()->setCellValueByColumnAndRow(15,$excel_row,$nodin->EXECUTOR_PIC_EMAIL);
			 $object->getActiveSheet()->setCellValueByColumnAndRow(16,$excel_row,$nodin->NETWORLELEMENT_NAME);
			 $object->getActiveSheet()->setCellValueByColumnAndRow(17,$excel_row,$nodin->USER_ACCESS_LOGIN);
			 $object->getActiveSheet()->setCellValueByColumnAndRow(18,$excel_row,$nodin->NE_TYPE);
			 $object->getActiveSheet()->setCellValueByColumnAndRow(19,$excel_row,$nodin->AREA_NE);
			 $object->getActiveSheet()->setCellValueByColumnAndRow(20,$excel_row,$nodin->ADDITIONAL_INFORMATION);
			 $object->getActiveSheet()->setCellValueByColumnAndRow(21,$excel_row,$nodin->ACCESS_LEVEL);
			 $object->getActiveSheet()->setCellValueByColumnAndRow(22,$excel_row,$nodin->ROLEKEYS);
			 $object->getActiveSheet()->setCellValueByColumnAndRow(23,$excel_row,$nodin->RESPONSIBLE_APPROVAL);
			 $object->getActiveSheet()->setCellValueByColumnAndRow(24,$excel_row,$nodin->DETAILED_LOCATION);
			 $object->getActiveSheet()->setCellValueByColumnAndRow(25,$excel_row,$nodin->NE_NAME_IMPACTED_LIST);
			 $object->getActiveSheet()->setCellValueByColumnAndRow(26,$excel_row,$nodin->NE_IMPACT_DESCRIPTION);
			 $object->getActiveSheet()->setCellValueByColumnAndRow(27,$excel_row,$nodin->NE_OUTAGE_DURATION_MIN);
			 $object->getActiveSheet()->setCellValueByColumnAndRow(28,$excel_row,$nodin->AFFECTED_SERVICE_DESCRIPTION);
			 $object->getActiveSheet()->setCellValueByColumnAndRow(29,$excel_row,$nodin->SEVICE_OUTAGE_DURATION_MIN);
			 $object->getActiveSheet()->setCellValueByColumnAndRow(30,$excel_row,$nodin->NE_ID_PRIMARY);
			 $object->getActiveSheet()->setCellValueByColumnAndRow(31,$excel_row,$nodin->NE_NAME_PRIMARY);
			 $object->getActiveSheet()->setCellValueByColumnAndRow(32,$excel_row,$nodin->BTS_NAME);
			 $object->getActiveSheet()->setCellValueByColumnAndRow(33,$excel_row,$nodin->FREQUENCY);
			 $object->getActiveSheet()->setCellValueByColumnAndRow(34,$excel_row,$nodin->AFFECTED_SERVICE);
			 $object->getActiveSheet()->setCellValueByColumnAndRow(35,$excel_row,$nodin->PROVINSI);
			 $object->getActiveSheet()->setCellValueByColumnAndRow(36,$excel_row,$nodin->KELURAHAN);
			 $object->getActiveSheet()->setCellValueByColumnAndRow(37,$excel_row,$nodin->KECAMATAN);
			 $object->getActiveSheet()->setCellValueByColumnAndRow(38,$excel_row,$nodin->CLUSTER);
			 $object->getActiveSheet()->setCellValueByColumnAndRow(39,$excel_row,$nodin->CITY);
		 $excel_row++;
		}

		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Nodin Swap.xls"');
		$object_writer->save('php://output');
		redirect('NodinController/index_nodin');
	}

	public function set_nodinid(){
		if($this->input->post('checkbox')){
			$check = "";
			$region = "";
			$regional = "";
			$end_time = "";
			$activity = "";
			$activity2 = "";
			$att_tier2 = "";
			$start_time = "";
			$filename_mop = "";
			$filename_timeline = "";
			$posted = $this->input->post();

			if (isset($_SESSION['regional'])) {
				for ($r=11; $r > 0 ; $r--) {
					if ('REGIONAL'.$r == $_SESSION['regional']) {
						$regional = "r".$r;
					}
				}
			}

			if(isset($_SESSION['regional'])) {
				switch($_SESSION['regional']) {
					case 'REGIONAL1':
						$region = "R1 Sumbagut;";
						break;
					case 'REGIONAL2':
						$region = "R2 Sumbagsel;";
						break;
					case 'REGIONAL3';
						$region = "R3 Jabotabek;";
						break;
					case 'REGIONAL4':
						$region = "R4 Jawabarat;";
						break;
					case 'REGIONAL5':
						$region = "R5 Jawatengah;";
						break;
					case 'REGIONAL6':
						$regoin = "R6 Jawatimur;";
						break;
					case 'REGIONAL7':
						$region = "R7 Balinusra;";
						break;
					case 'REGIONAL8';
						$region = "R8 Kalimantan;";
						break;
					case 'REGIONAL9';
						$region = "R9 Sulawesi;";
						break;
					case 'REGIONAL10':
						$region = "R10 Sumbagteng;";
						break;
					case 'REGOINAL11':
					 $region = "R11 Puma;";
					 break;
					default:
						$region = "NULL;";
						break;
				}
			}

			//set start date
			$datetime = new DateTime(date('Y-m-d'));
			$datetime->modify('+4 day');
			$start_time = $datetime->format('Y-m-d');

			//set end date
			$datetime = new DateTime(date('Y-m-d'));
			$datetime->modify('+7 day');
			$end_time = $datetime->format('Y-m-d');

			//upload file mop
			if($this->upload->do_upload('file_mop')) {
				$upload_mop = $this->upload->data();
				$filename_mop = $upload_mop['file_name'];
			}else{
				redirect('NodinController/index_nodin?remedy=Gagal upload file mop');
			}

			//upload file timeline
			if($this->upload->do_upload('file_timeline')) {
				$upload_timeline = $this->upload->data();
				$filename_timeline = $upload_timeline['file_name'];
			}else {
				redirect('NodinController/index_nodin?remedy=Gagal upload file timenline');
			}

			foreach($posted['checkbox'] as $key => $post) {
				for ($i=count($posted['CELL_NAME']); $i > 0 ; $i--) {
					if($key == $i) {
						if($posted['BAND'][$i] == '2G') {
							$att_tier2 = "BTS";
						}elseif($posted['BAND'][$i] == '3G') {
							$att_tier2 = "Node B";
						}else{
							$att_tier2 = "eNode B";
						}

						if($posted['BAND'][$i] == '2G') {
							$activity = "Swap BTS Retail";
						}elseif($posted['BAND'][$i] == '3G') {
							$activity = "Swap Node B Retail";
						}else{
							$activity = "Swap eNode B Retail";
						}

						if($posted['BAND'][$i] == '2G') {
							$activity2 = "Swap BTS";
						}elseif($posted['BAND'][$i] == '3G') {
							$activity2 = "Swap Node B";
						}else{
							$activity2 = "Swap eNode B";
						}

						ini_set('max_execution_time', 9000);
						ini_set('default_socket_timeout', 9000);
						// $api_url = 'http://10.2.114.175:8080/arsys/services/ARService?server=remedynodapp3&webService=TSEL_CHG_NO_Change_Create';
						$api_url = 'http://10.2.114.175:8080/arsys/WSDL/public/remedynodapp3/TSEL_CHG_NO_Change_Create';
						$api_username = 'int_btsoa';
						$api_password = '123456';

						$vendor_name = "";
						$content_affected_SMS = "";
						$content_affected_MMS = "";
						$content_affected_DATA = "";
						$content_affected_VOICE = "";

						$affected = explode(";",$posted['AFFECTED_SERVICE'][$i]);
						if(count($affected) != 0) {
							foreach ($affected as $affect => $value) {
								if($value == 'SMS') {
									$content_affected_SMS = '<urn:as_'.$value.'>0</urn:as_'.$value.'>';
								}elseif($value == 'MMS') {
									$content_affected_MMS = '<urn:as_'.$value.'>0</urn:as_'.$value.'>';
								}elseif($value == 'DATA') {
									$content_affected_DATA = '<urn:as_'.$value.'>0</urn:as_'.$value.'>';
								}elseif($value == 'VOICE') {
									$content_affected_VOICE = '<urn:as_'.$value.'>0</urn:as_'.$value.'>';
								}
							}
						}

						if($posted['VENDOR'][$i] == "NOKIA") {
							$vendor_name = 'nsn';
						}elseif($posted['VENDOR'][$i] == "HUAWEI") {
							$vendor_name = 'huawei';
						}elseif($posted['VENDOR'][$i] == "ZTE") {
							$vendor_name = 'zte';
						}elseif($posted['VENDOR'][$i] == "ERICSSON") {
							$vendor_name = 'ericsson';
						}

						if($posted['setting'] == 'standard') {
							if($posted['BAND'][$i] == '4G') {
								$params = '
								<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:TSEL_CHG_NO_Change_Create">
									 <soapenv:Header>
											<urn:AuthenticationInfo>
												 <urn:userName>int_btsoa</urn:userName>
												 <urn:password>123456</urn:password>
											</urn:AuthenticationInfo>
									 </soapenv:Header>
									 <soapenv:Body>
											<urn:opCreate>
												 <urn:Operational_Category_Tier_1>AI</urn:Operational_Category_Tier_1>
												 <urn:Operational_Category_Tier_2>'.$att_tier2.'</urn:Operational_Category_Tier_2>
												 <urn:Operational_Category_Tier_3>New Integration</urn:Operational_Category_Tier_3>
												 <urn:Activity>'.$activity.'</urn:Activity>
												 <urn:Description_of_Change>New Site Integration + '.$posted["SITE_NAME"][$i].' + '.$posted["NE_ID"][$i].' + Djatnika 15:05.</urn:Description_of_Change>
												 <urn:Reason>'.$posted["REASON_FOR_CHANGE"][$i].'</urn:Reason>
 												 <urn:Requestor_ID>'.$_SESSION["user"].'</urn:Requestor_ID>
												 <urn:Region>'.$region.'</urn:Region>
												 <urn:Responsible_Approval>IT Operation Jabotabek</urn:Responsible_Approval>
												 <urn:Detailed_Location>'.$posted["DETAILED_LOCATION"][$i].'</urn:Detailed_Location>
												 <!--0 = Yes, 1 = No-->
												 <urn:Impact_to_NE>0</urn:Impact_to_NE>
												 <urn:NE_NAME_IMPACTED_LIST>'.$posted["NE_NAME_IMPACTED_LIST"][$i].'</urn:NE_NAME_IMPACTED_LIST>
												 <urn:NE_Impact_Description>'.$posted["NE_IMPACT_DESCRIPTION"][$i].'</urn:NE_Impact_Description>
												 <urn:NE_Outage_Duration__minute_>'.$posted["NE_OUTAGE_DURATION_MIN"][$i].'</urn:NE_Outage_Duration__minute_>
												 <urn:Affected_Service_Description>None</urn:Affected_Service_Description>
												 <urn:Service_Outage_Duration__minute_>60</urn:Service_Outage_Duration__minute_>
												 <urn:NE_ID>'.$posted["NE_ID"][$i].'</urn:NE_ID>
												 <urn:NE_Name>'.$posted["NETWORLELEMENT_NAME"][$i].'</urn:NE_Name>
												 <urn:Execution_Start_Time>'.$start_time."T".date("H:m:s").'</urn:Execution_Start_Time>
												 <urn:Execution_End_Time>'.$end_time."T".date("H:m:s").'</urn:Execution_End_Time>
												 <urn:MOP_attachmentName>'.$filename_mop.'</urn:MOP_attachmentName>
												 <urn:MOP_attachmentData>'.base64_encode(file_get_contents('./file_upload/'.$filename_mop)).'</urn:MOP_attachmentData>
												 <urn:Timeline_attachmentName>'.$filename_timeline.'</urn:Timeline_attachmentName>
												 <urn:Timeline_attachmentData>'.base64_encode(file_get_contents('./file_upload/'.$filename_timeline)).'</urn:Timeline_attachmentData>
												 <urn:PO_NUMBER__c>Nodin RNP 049-2018</urn:PO_NUMBER__c>
												 <urn:OIC_Site_ID>BGE914</urn:OIC_Site_ID>
												 <urn:OIC_Site_Name>'.$posted["SITE_NAME"][$i].'</urn:OIC_Site_Name>
												 <urn:OIC_Cell_Name>'.$posted["CELL_NAME"][$i].'</urn:OIC_Cell_Name>
												 <urn:OIC_BSC_ID>'.$posted["BSC_ID"][$i].'</urn:OIC_BSC_ID>
												 <urn:OIC_BSC_Name>'.$posted["BSC_NAME"][$i].'</urn:OIC_BSC_Name>
												 <urn:OIC_Network_Type>'.$posted["BAND"][$i].'</urn:OIC_Network_Type>
												 <urn:OIC_LAC>'.$posted["NEW_TAC"][$i].'</urn:OIC_LAC>
												 <urn:OIC_CI>'.$posted["NEW_CI"][$i].'</urn:OIC_CI>
												 <urn:OIC_Longitude>'.$posted["LONGITUDE"][$i].'</urn:OIC_Longitude>
												 <urn:OIC_Latitude>'.$posted["LATITUDE"][$i].'</urn:OIC_Latitude>
												 <urn:OIC_Vendor_Name>'.$vendor_name.'</urn:OIC_Vendor_Name>
												 <urn:OIC_Province>sample province</urn:OIC_Province>
												 <urn:OIC_City>sample kota</urn:OIC_City>
												 <urn:OIC_Kecamatan>sample kecamatan</urn:OIC_Kecamatan>
												 <urn:OIC_Kelurahan>sample kelurahan</urn:OIC_Kelurahan>
												 <urn:OIC_Address>sample address</urn:OIC_Address>
												 <urn:OIC_Cluster>sample cluster</urn:OIC_Cluster>
												 <urn:AIExecutor>
													 <urn:Executor_Instance>v</urn:Executor_Instance>
													 <urn:Executor_PIC_Company>'.$posted['EXECUTOR_PIC_COMPANY'][$i].'</urn:Executor_PIC_Company>
													 <urn:Executor_PIC_Name>'.$posted['EXECUTOR_PIC_NAME'][$i].'</urn:Executor_PIC_Name>
													 <urn:Executor_PIC_Phone>'.$posted['EXECUTOR_PIC_PHONE'][$i].'</urn:Executor_PIC_Phone>
													 <!--Optional:-->
													 <urn:Executor_PIC_Email>'.$posted['EXECUTOR_PIC_EMAIL'][$i].'</urn:Executor_PIC_Email>
													 <urn:NE_Name>'.$att_tier2.'</urn:NE_Name>
													 <urn:User_Access_Login>'.$posted['USER_ACCESS_LOGIN'][$i].'</urn:User_Access_Login>
													 <urn:NE_Type>'.$posted['NE_TYPE'][$i].'</urn:NE_Type>
													 <urn:Region_NE>'.$regional.'</urn:Region_NE>
													 <urn:Area_NE>'.$posted['AREA_NE'][$i].'</urn:Area_NE>
													 <!--Optional:-->
													 <urn:Additional_Information>'.$posted['ADDITIONAL_INFORMATION'][$i].'</urn:Additional_Information>
													 <urn:Vendor_Name>'.$vendor_name.'</urn:Vendor_Name>
													 <urn:Access_Level>'.$posted['ACCESS_LEVEL'][$i].'</urn:Access_Level>
													 <urn:Network_Type>'.$posted['BAND'][$i].'</urn:Network_Type>
												</urn:AIExecutor>
											</urn:opCreate>
									 </soapenv:Body>
								</soapenv:Envelope>
								';
							}else{
								$params = '
								<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:TSEL_CHG_NO_Change_Create">
									 <soapenv:Header>
											<urn:AuthenticationInfo>
												 <urn:userName>int_btsoa</urn:userName>
												 <urn:password>123456</urn:password>
											</urn:AuthenticationInfo>
									 </soapenv:Header>
									 <soapenv:Body>
											<urn:opCreate>
												 <urn:Operational_Category_Tier_1>AI</urn:Operational_Category_Tier_1>
												 <urn:Operational_Category_Tier_2>'.$att_tier2.'</urn:Operational_Category_Tier_2>
												 <urn:Operational_Category_Tier_3>New Integration</urn:Operational_Category_Tier_3>
												 <urn:Activity>'.$activity.'</urn:Activity>
												 <urn:Description_of_Change>New Site Integration + '.$posted["SITE_NAME"][$i].' + '.$posted["NE_ID"][$i].' + Djatnika 15:05.</urn:Description_of_Change>
												 <urn:Reason>'.$posted["REASON_FOR_CHANGE"][$i].'</urn:Reason>
 												 <urn:Requestor_ID>ikarah</urn:Requestor_ID>
												 <urn:Region>R3 Jabotabek;</urn:Region>
												 <urn:Responsible_Approval>IT Operation Jabotabek</urn:Responsible_Approval>
												 <urn:Detailed_Location>'.$posted["DETAILED_LOCATION"][$i].'</urn:Detailed_Location>
												 <!--0 = Yes, 1 = No-->
												 <urn:Impact_to_NE>0</urn:Impact_to_NE>
												 <urn:NE_Name_Impacted>'.$posted["NE_NAME_IMPACTED_LIST"][$i].'</urn:NE_Name_Impacted>
												 <urn:NE_Impact_Description>'.$posted["NE_IMPACT_DESCRIPTION"][$i].'</urn:NE_Impact_Description>
												 <urn:NE_Outage_Duration__minute_>'.$posted["NE_OUTAGE_DURATION_MIN"][$i].'</urn:NE_Outage_Duration__minute_>
												 <urn:Affected_Service_Description>None</urn:Affected_Service_Description>
												 <urn:Service_Outage_Duration__minute_>60</urn:Service_Outage_Duration__minute_>
												 <urn:NE_ID>'.$posted["NE_ID"][$i].'</urn:NE_ID>
												 <urn:NE_Name>'.$posted["NETWORLELEMENT_NAME"][$i].'</urn:NE_Name>
												 <urn:Execution_Start_Time>'.$start_time."T".date("H:m:s").'</urn:Execution_Start_Time>
												 <urn:Execution_End_Time>'.$end_time."T".date("H:m:s").'</urn:Execution_End_Time>
												 <urn:MOP_attachmentName>'.$filename_mop.'</urn:MOP_attachmentName>
												 <urn:MOP_attachmentData>'.base64_encode(file_get_contents('./file_upload/'.$filename_mop)).'</urn:MOP_attachmentData>
												 <urn:Timeline_attachmentName>'.$filename_timeline.'</urn:Timeline_attachmentName>
												 <urn:Timeline_attachmentData>'.base64_encode(file_get_contents('./file_upload/'.$filename_timeline)).'</urn:Timeline_attachmentData>
												 <urn:PO_NUMBER__c>Nodin RNP 049-2018</urn:PO_NUMBER__c>
												 <urn:OIC_Site_ID>BGE914</urn:OIC_Site_ID>
												 <urn:OIC_Site_Name>'.$posted["SITE_NAME"][$i].'</urn:OIC_Site_Name>
												 <urn:OIC_Cell_Name>'.$posted["CELL_NAME"][$i].'</urn:OIC_Cell_Name>
												 <urn:OIC_BSC_ID>'.$posted["BSC_ID"][$i].'</urn:OIC_BSC_ID>
												 <urn:OIC_BSC_Name>'.$posted["BSC_NAME"][$i].'</urn:OIC_BSC_Name>
												 <urn:OIC_Network_Type>'.$posted["BAND"][$i].'</urn:OIC_Network_Type>
												 <urn:OIC_LAC>'.$posted["NEW_LAC"][$i].'</urn:OIC_LAC>
												 <urn:OIC_CI>'.$posted["NEW_CI"][$i].'</urn:OIC_CI>
												 <urn:OIC_Longitude>'.$posted["LONGITUDE"][$i].'</urn:OIC_Longitude>
												 <urn:OIC_Latitude>'.$posted["LATITUDE"][$i].'</urn:OIC_Latitude>
												 <urn:OIC_Vendor_Name>'.$vendor_name.'</urn:OIC_Vendor_Name>
												 <urn:OIC_Province>sample province</urn:OIC_Province>
												 <urn:OIC_City>sample kota</urn:OIC_City>
												 <urn:OIC_Kecamatan>sample kecamatan</urn:OIC_Kecamatan>
												 <urn:OIC_Kelurahan>sample kelurahan</urn:OIC_Kelurahan>
												 <urn:OIC_Address>sample address</urn:OIC_Address>
												 <urn:OIC_Cluster>sample cluster</urn:OIC_Cluster>
												 <urn:AIExecutor>
													 <urn:Executor_Instance>v</urn:Executor_Instance>
													 <urn:Executor_PIC_Company>'.$posted['EXECUTOR_PIC_COMPANY'][$i].'</urn:Executor_PIC_Company>
													 <urn:Executor_PIC_Name>'.$posted['EXECUTOR_PIC_NAME'][$i].'</urn:Executor_PIC_Name>
													 <urn:Executor_PIC_Phone>'.$posted['EXECUTOR_PIC_PHONE'][$i].'</urn:Executor_PIC_Phone>
													 <!--Optional:-->
													 <urn:Executor_PIC_Email>'.$posted['EXECUTOR_PIC_EMAIL'][$i].'</urn:Executor_PIC_Email>
													 <urn:NE_Name>'.$att_tier2.'</urn:NE_Name>
													 <urn:User_Access_Login>'.$posted['USER_ACCESS_LOGIN'][$i].'</urn:User_Access_Login>
													 <urn:NE_Type>'.$posted['NE_TYPE'][$i].'</urn:NE_Type>
													 <urn:Region_NE>r3</urn:Region_NE>
													 <urn:Area_NE>'.$posted['AREA_NE'][$i].'</urn:Area_NE>
													 <!--Optional:-->
													 <urn:Additional_Information>'.$posted['ADDITIONAL_INFORMATION'][$i].'</urn:Additional_Information>
													 <urn:Vendor_Name>'.$vendor_name.'</urn:Vendor_Name>
													 <urn:Access_Level>'.$posted['ACCESS_LEVEL'][$i].'</urn:Access_Level>
													 <urn:Network_Type>'.$posted['BAND'][$i].'</urn:Network_Type>
												</urn:AIExecutor>
											</urn:opCreate>
									 </soapenv:Body>
								</soapenv:Envelope>
								';
							}
						}else{
							if($posted['BAND'][$i] == '4G') {
								$params = '
								<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:TSEL_CHG_NO_Change_Create">
									 <soapenv:Header>
											<urn:AuthenticationInfo>
												 <urn:userName>int_btsoa</urn:userName>
												 <urn:password>123456</urn:password>
											</urn:AuthenticationInfo>
									 </soapenv:Header>
									 <soapenv:Body>
											<urn:opCreate>
												 <urn:Operational_Category_Tier_1>AI</urn:Operational_Category_Tier_1>
												 <urn:Operational_Category_Tier_2>'.$att_tier2.'</urn:Operational_Category_Tier_2>
												 <urn:Operational_Category_Tier_3>New Integration</urn:Operational_Category_Tier_3>
												 <urn:Activity>'.$activity2.'</urn:Activity>
												 <urn:Description_of_Change>New Site Integration + '.$posted["SITE_NAME"][$i].' + '.$posted["NE_ID"][$i].' + Djatnika 15:05.</urn:Description_of_Change>
												 <urn:Reason>'.$posted["REASON_FOR_CHANGE"][$i].'</urn:Reason>
 												 <urn:Requestor_ID>'.$_SESSION["user"].'</urn:Requestor_ID>
												 <urn:Region>'.$region.'</urn:Region>
												 <urn:Responsible_Approval>IT Operation Jabotabek</urn:Responsible_Approval>
												 <urn:Detailed_Location>'.$posted["DETAILED_LOCATION"][$i].'</urn:Detailed_Location>
												 <!--0 = Yes, 1 = No-->
												 <urn:Impact_to_NE>0</urn:Impact_to_NE>
												 <urn:NE_NAME_IMPACTED_LIST>'.$posted["NE_NAME_IMPACTED_LIST"][$i].'</urn:NE_NAME_IMPACTED_LIST>
												 <urn:NE_Impact_Description>'.$posted["NE_IMPACT_DESCRIPTION"][$i].'</urn:NE_Impact_Description>
												 <urn:NE_Outage_Duration__minute_>'.$posted["NE_OUTAGE_DURATION_MIN"][$i].'</urn:NE_Outage_Duration__minute_>
												 '.$content_affected_SMS.'
												 '.$content_affected_MMS.'
												 '.$content_affected_DATA.'
												 '.$content_affected_VOICE.'
												 <urn:Affected_Service_Description>'.$posted["AFFECTED_SERVICE_DESCRIPTION"][$i].'</urn:Affected_Service_Description>
												 <urn:Service_Outage_Duration__minute_>'.$posted["SERVICE_OUTAGE_DESCRIPTION_MIN"][$i].'</urn:Service_Outage_Duration__minute_>
												 <urn:NE_ID>'.$posted["NE_ID"][$i].'</urn:NE_ID>
												 <urn:NE_Name>'.$posted["NETWORLELEMENT_NAME"][$i].'</urn:NE_Name>
												 <urn:Execution_Start_Time>'.$start_time."T".date("H:m:s").'</urn:Execution_Start_Time>
												 <urn:Execution_End_Time>'.$end_time."T".date("H:m:s").'</urn:Execution_End_Time>
												 <urn:MOP_attachmentName>'.$filename_mop.'</urn:MOP_attachmentName>
												 <urn:MOP_attachmentData>'.base64_encode(file_get_contents('./file_upload/'.$filename_mop)).'</urn:MOP_attachmentData>
												 <urn:Timeline_attachmentName>'.$filename_timeline.'</urn:Timeline_attachmentName>
												 <urn:Timeline_attachmentData>'.base64_encode(file_get_contents('./file_upload/'.$filename_timeline)).'</urn:Timeline_attachmentData>
												 <urn:PO_NUMBER__c>Nodin RNP 049-2018</urn:PO_NUMBER__c>
												 <urn:OIC_Site_ID>'.$posted["SITE_ID"][$i].'</urn:OIC_Site_ID>
												 <urn:OIC_Site_Name>'.$posted["SITE_NAME"][$i].'</urn:OIC_Site_Name>
												 <urn:OIC_Cell_Name>'.$posted["CELL_NAME"][$i].'</urn:OIC_Cell_Name>
												 <urn:OIC_BSC_ID>'.$posted["BSC_ID"][$i].'</urn:OIC_BSC_ID>
												 <urn:OIC_BSC_Name>'.$posted["BSC_NAME"][$i].'</urn:OIC_BSC_Name>
												 <urn:OIC_Network_Type>'.$posted["BAND"][$i].'</urn:OIC_Network_Type>
												 <urn:OIC_LAC>'.$posted["NEW_TAC"][$i].'</urn:OIC_LAC>
												 <urn:OIC_CI>'.$posted["NEW_CI"][$i].'</urn:OIC_CI>
												 <urn:OIC_Longitude>'.$posted["LONGITUDE"][$i].'</urn:OIC_Longitude>
												 <urn:OIC_Latitude>'.$posted["LATITUDE"][$i].'</urn:OIC_Latitude>
												 <urn:OIC_Vendor_Name>'.$vendor_name.'</urn:OIC_Vendor_Name>
												 <urn:OIC_Province>sample province</urn:OIC_Province>
												 <urn:OIC_City>sample kota</urn:OIC_City>
												 <urn:OIC_Kecamatan>sample kecamatan</urn:OIC_Kecamatan>
												 <urn:OIC_Kelurahan>sample kelurahan</urn:OIC_Kelurahan>
												 <urn:OIC_Address>sample address</urn:OIC_Address>
												 <urn:OIC_Cluster>sample cluster</urn:OIC_Cluster>
												 <urn:AIExecutor>
													 <urn:Executor_Instance>v</urn:Executor_Instance>
													 <urn:Executor_PIC_Company>'.$posted['EXECUTOR_PIC_COMPANY'][$i].'</urn:Executor_PIC_Company>
													 <urn:Executor_PIC_Name>'.$posted['EXECUTOR_PIC_NAME'][$i].'</urn:Executor_PIC_Name>
													 <urn:Executor_PIC_Phone>'.$posted['EXECUTOR_PIC_PHONE'][$i].'</urn:Executor_PIC_Phone>
													 <!--Optional:-->
													 <urn:Executor_PIC_Email>'.$posted['EXECUTOR_PIC_EMAIL'][$i].'</urn:Executor_PIC_Email>
													 <urn:NE_Name>'.$att_tier2.'</urn:NE_Name>
													 <urn:User_Access_Login>'.$posted['USER_ACCESS_LOGIN'][$i].'</urn:User_Access_Login>
													 <urn:NE_Type>'.$posted['NE_TYPE'][$i].'</urn:NE_Type>
													 <urn:Region_NE>'.$regional.'</urn:Region_NE>
													 <urn:Area_NE>'.$posted['AREA_NE'][$i].'</urn:Area_NE>
													 <!--Optional:-->
													 <urn:Additional_Information>'.$posted['ADDITIONAL_INFORMATION'][$i].'</urn:Additional_Information>
													 <urn:Vendor_Name>'.$vendor_name.'</urn:Vendor_Name>
													 <urn:Access_Level>'.$posted['ACCESS_LEVEL'][$i].'</urn:Access_Level>
													 <urn:Network_Type>'.$posted['BAND'][$i].'</urn:Network_Type>
												</urn:AIExecutor>
											</urn:opCreate>
									 </soapenv:Body>
								</soapenv:Envelope>
								';
							}else{
								$params = '
								<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:TSEL_CHG_NO_Change_Create">
									 <soapenv:Header>
											<urn:AuthenticationInfo>
												 <urn:userName>int_btsoa</urn:userName>
												 <urn:password>123456</urn:password>
											</urn:AuthenticationInfo>
									 </soapenv:Header>
									 <soapenv:Body>
											<urn:opCreate>
												 <urn:Operational_Category_Tier_1>AI</urn:Operational_Category_Tier_1>
												 <urn:Operational_Category_Tier_2>'.$att_tier2.'</urn:Operational_Category_Tier_2>
												 <urn:Operational_Category_Tier_3>New Integration</urn:Operational_Category_Tier_3>
												 <urn:Activity>'.$activity2.'</urn:Activity>
												 <urn:Description_of_Change>New Site Integration + '.$posted["SITE_NAME"][$i].' + '.$posted["NE_ID"][$i].' + Djatnika 15:05.</urn:Description_of_Change>
												 <urn:Reason>'.$posted["REASON_FOR_CHANGE"][$i].'</urn:Reason>
 												 <urn:Requestor_ID>'.$_SESSION["user"].'</urn:Requestor_ID>
												 <urn:Region>'.$region.'</urn:Region>
												 <urn:Responsible_Approval>IT Operation Jabotabek</urn:Responsible_Approval>
												 <urn:Detailed_Location>'.$posted["DETAILED_LOCATION"][$i].'</urn:Detailed_Location>
												 <!--0 = Yes, 1 = No-->
												 <urn:Impact_to_NE>0</urn:Impact_to_NE>
												 <urn:NE_Name_Impacted>'.$posted["NE_NAME_IMPACTED_LIST"][$i].'</urn:NE_Name_Impacted>
												 <urn:NE_Impact_Description>'.$posted["NE_IMPACT_DESCRIPTION"][$i].'</urn:NE_Impact_Description>
												 <urn:NE_Outage_Duration__minute_>'.$posted["NE_OUTAGE_DURATION_MIN"][$i].'</urn:NE_Outage_Duration__minute_>
												 '.$content_affected_SMS.'
												 '.$content_affected_MMS.'
												 '.$content_affected_DATA.'
												 '.$content_affected_VOICE.'
												 <urn:Affected_Service_Description>'.$posted["AFFECTED_SERVICE_DESCRIPTION"][$i].'</urn:Affected_Service_Description>
												 <urn:Service_Outage_Duration__minute_>'.$posted["SERVICE_OUTAGE_DESCRIPTION_MIN"][$i].'</urn:Service_Outage_Duration__minute_>
												 <urn:NE_ID>'.$posted["NE_ID"][$i].'</urn:NE_ID>
												 <urn:NE_Name>'.$posted["NETWORLELEMENT_NAME"][$i].'</urn:NE_Name>
												 <urn:Execution_Start_Time>'.$start_time."T".date("H:m:s").'</urn:Execution_Start_Time>
												 <urn:Execution_End_Time>'.$end_time."T".date("H:m:s").'</urn:Execution_End_Time>
												 <urn:MOP_attachmentName>'.$filename_mop.'</urn:MOP_attachmentName>
												 <urn:MOP_attachmentData>'.base64_encode(file_get_contents("./file_upload/".$filename_mop)).'</urn:MOP_attachmentData>
												 <urn:Timeline_attachmentName>'.$filename_timeline.'</urn:Timeline_attachmentName>
												 <urn:Timeline_attachmentData>'.base64_encode(file_get_contents("./file_upload/".$filename_timeline)).'</urn:Timeline_attachmentData>
												 <urn:PO_NUMBER__c>Nodin RNP 049-2018</urn:PO_NUMBER__c>
												 <urn:OIC_Site_ID>'.$posted["SITE_ID"][$i].'</urn:OIC_Site_ID>
												 <urn:OIC_Site_Name>'.$posted["SITE_NAME"][$i].'</urn:OIC_Site_Name>
												 <urn:OIC_Cell_Name>'.$posted["CELL_NAME"][$i].'</urn:OIC_Cell_Name>
												 <urn:OIC_BSC_ID>'.$posted["BSC_ID"][$i].'</urn:OIC_BSC_ID>
												 <urn:OIC_BSC_Name>'.$posted["BSC_NAME"][$i].'</urn:OIC_BSC_Name>
												 <urn:OIC_Network_Type>'.$posted["BAND"][$i].'</urn:OIC_Network_Type>
												 <urn:OIC_LAC>'.$posted["NEW_LAC"][$i].'</urn:OIC_LAC>
												 <urn:OIC_CI>'.$posted["NEW_CI"][$i].'</urn:OIC_CI>
												 <urn:OIC_Longitude>'.$posted["LONGITUDE"][$i].'</urn:OIC_Longitude>
												 <urn:OIC_Latitude>'.$posted["LATITUDE"][$i].'</urn:OIC_Latitude>
												 <urn:OIC_Vendor_Name>'.$vendor_name.'</urn:OIC_Vendor_Name>
												 <urn:OIC_Province>sample province</urn:OIC_Province>
												 <urn:OIC_City>sample kota</urn:OIC_City>
												 <urn:OIC_Kecamatan>sample kecamatan</urn:OIC_Kecamatan>
												 <urn:OIC_Kelurahan>sample kelurahan</urn:OIC_Kelurahan>
												 <urn:OIC_Address>sample address</urn:OIC_Address>
												 <urn:OIC_Cluster>sample cluster</urn:OIC_Cluster>
												 <urn:AIExecutor>
													 <urn:Executor_Instance>v</urn:Executor_Instance>
													 <urn:Executor_PIC_Company>'.$posted['EXECUTOR_PIC_COMPANY'][$i].'</urn:Executor_PIC_Company>
													 <urn:Executor_PIC_Name>'.$posted['EXECUTOR_PIC_NAME'][$i].'</urn:Executor_PIC_Name>
													 <urn:Executor_PIC_Phone>'.$posted['EXECUTOR_PIC_PHONE'][$i].'</urn:Executor_PIC_Phone>
													 <!--Optional:-->
													 <urn:Executor_PIC_Email>'.$posted['EXECUTOR_PIC_EMAIL'][$i].'</urn:Executor_PIC_Email>
													 <urn:NE_Name>'.$att_tier2.'</urn:NE_Name>
													 <urn:User_Access_Login>'.$posted['USER_ACCESS_LOGIN'][$i].'</urn:User_Access_Login>
													 <urn:NE_Type>'.$posted['NE_TYPE'][$i].'</urn:NE_Type>
													 <urn:Region_NE>'.$regional.'</urn:Region_NE>
													 <urn:Area_NE>'.$posted['AREA_NE'][$i].'</urn:Area_NE>
													 <!--Optional:-->
													 <urn:Additional_Information>'.$posted['ADDITIONAL_INFORMATION'][$i].'</urn:Additional_Information>
													 <urn:Vendor_Name>'.$vendor_name.'</urn:Vendor_Name>
													 <urn:Access_Level>'.$posted['ACCESS_LEVEL'][$i].'</urn:Access_Level>
													 <urn:Network_Type>'.$posted['BAND'][$i].'</urn:Network_Type>
												</urn:AIExecutor>
											</urn:opCreate>
									 </soapenv:Body>
								</soapenv:Envelope>
								';
							}
						}

						// echo $params;
						$check = json_decode($this->curl->simple_get($this->API_Check.'?ne_id='.$posted['NE_ID'][$i]));
						if($api_url != '' AND count($check) != 0){
							$result = $this->nusoap_library->soaprequest($api_url, $api_username, $api_password, $params);
							$crq = $result;

							if($posted['setting'] == "standard") {
								$remedy = array(
									'crq'					=> $crq,
									'cell_name'		=> $posted['CELL_NAME'][$i],
									'mop'					=> $filename_mop,
									'timeline' 		=> $filename_timeline,
									'start_date'	=> $start_time."T".date("H:m:s")."+07:00",
									'end_date'		=> $end_time."T".date("H:m:s")."+07:00",
									'mcc'					=> 510,
									'mnc'					=> 10,
									'flag_nodin' 	=> 1
								);
							}else{
								$remedy = array(
									'crq'					=> $crq,
									'cell_name'		=> $posted['CELL_NAME'][$i],
									'mop'					=> $filename_mop,
									'timeline' 		=> $filename_timeline,
									'start_date'	=> $start_time."T".date("H:m:s")."+07:00",
									'end_date'		=> $end_time."T".date("H:m:s")."+07:00",
									'mcc'					=> 510,
									'mnc'					=> 10,
									'flag_nodin' 	=> 1
								);
							}

							$this->curl->simple_put($this->API_CRQ.'/CRQController', $remedy, array(CURLOPT_BUFFERSIZE => 10));
						}else{
							redirect('NodinController/index_nodin?remedy=Fail Validate From REMEDY');
						}
					}
				}
				$i++;
			}

			if(count($check) != 0) {
				$data = array(
					'FLAG_NODIN'   => 0, // Belum : 1, Sudah : 0
					'DESCRIPTION'	 => $posted['setting'],
					'SWAP'	 => $posted['swap']
				);

				$update =  $this->curl->simple_put($this->API_NoNodin.'/NoNodinController', $data, array(CURLOPT_BUFFERSIZE => 10));
				if($update){
					// $this->session->set_flashdata('hasil','Insert No Nodin Data Success');
				} else{
					// $this->session->set_flashdata('hasil','Insert No Nodin Data Fail');
				}

				$no = json_decode($this->curl->simple_get($this->API_GetNo.'/GetNoController'));
				$data_send = array();
				array_push($data_send,$data);
				foreach ($no as $no_ticket) {
					// $tiket = array(
					// 	'id_ticket'     => "SWAP/".date('Ymd')."/".str_pad($no_ticket->NO,3,'0',STR_PAD_LEFT),
					// 	'submitted_by'  => $_SESSION['username'],
					// 	'manager'   		=> $_SESSION['manager'],
					// 	'data'          => $data_send,
					// );

					$tiket = array(
						'id_ticket'     => "SWAP/".date('Ymd')."/".str_pad($no_ticket->NO,3,'0',STR_PAD_LEFT),
						'data'          => $data_send,
					);

					$workflowTiket = $this->curl->simple_post($this->API_Workflow.'/ManagerController',$tiket,array(CURLOPT_BUFFERSIZE => 10));
				}
			}else {
				echo "Gagal";
				redirect('NodinController/index_nodin?ne_id='.$posted['NE_ID']);
			}

			redirect('NodinController/index_listnodin?success=Create Nodin ID Success');
			//print_r($data);
		}
	}

	public function insert(){
		if($this->input->post('checkbox')){

			$posted = $this->input->post();
			foreach($posted as $post) {
				$data = array(
					'checkbox'  => $posted['checkbox'],
					'REGIONAL'  => $posted['REGIONAL'],
					'VENDOR'    => $posted['VENDOR'],
					'CELL_NAME' => $posted['CELL_NAME'],
					'LAC' 			=> $posted['LAC'],
					'TAC' 			=> $posted['TAC'],
					'CI'        => $posted['CI'],
					'NE_ID'     => $posted['NE_ID'],
					'SITE_ID'  	=> $posted['SITE_ID'],
					'BTS_NAME'	=> $posted['BTS_NAME'],
					'FREQUENCY'	=> $posted['FREQUENCY'],
					'LONGITUDE'	=> $posted['LONGITUDE'],
					'LATITUDE'	=> $posted['LATITUDE'],
					'BSC_NAME'	=> $posted['BSC_NAME'],
					'BSC_ID'		=> $posted['BSC_ID'],
					'SITE_NAME' => $posted['SITE_NAME']
				);
			}

			if($this->input->post('BAND') == "2G") {
				$insert = $this->curl->simple_post($this->API_Stylo2.'/Stylo2GController', $data, array(CURLOPT_BUFFERSIZE => 10));
			}elseif($this->input->post('BAND') == "3G") {
				$insert = $this->curl->simple_post($this->API_Stylo3.'/Stylo3GController', $data, array(CURLOPT_BUFFERSIZE => 10));
			}elseif($this->input->post('BAND') == "4G") {
				$insert = $this->curl->simple_post($this->API_Stylo4.'/Stylo4GController', $data, array(CURLOPT_BUFFERSIZE => 10));
			}

			if($insert){
				// $this->session->set_flashdata('hasil','Insert Data Success');
			} else{
				// $this->session->set_flashdata('hasil','Insert Data Fail');
			}

			redirect('NodinController/index_nodin');
		} else{
			$this->load->view('tambah');
		}
	}

	function import(){
		if(isset($_FILES["file"]["name"])){
			$path = $_FILES["file"]["tmp_name"];
			$object = PHPExcel_IOFactory::load($path);
			foreach($object->getWorksheetIterator() as $worksheet){
				$highestRow = $worksheet->getHighestRow();
				$highestColumn = $worksheet->getHighestColumn();
				for($row=2; $row<=$highestRow; $row++){
					if($this->input->post('band') == "2G") {
						$CELL_NAME = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
						$VENDOR = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
						$REGIONAL = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
						$LAC = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
						$CI = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
						$NEW_LAC = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
						$NEW_CI = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
						$SITE_ID = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
						$NE_ID = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
						$BAND = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
						$DESCRIPTION_OF_CHANGE = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
						$REASON_FOR_CHANGE = $worksheet->getCellByColumnAndRow(11, $row)->getValue();
						$EXECUTOR_PIC_COMPANY = $worksheet->getCellByColumnAndRow(12, $row)->getValue();
						$EXECUTOR_PIC_NAME = $worksheet->getCellByColumnAndRow(13, $row)->getValue();
						$EXECUTOR_PIC_PHONE = $worksheet->getCellByColumnAndRow(14, $row)->getValue();
						$EXECUTOR_PIC_EMAIL = $worksheet->getCellByColumnAndRow(15, $row)->getValue();
						$NETWORLELEMENT_NAME = $worksheet->getCellByColumnAndRow(16, $row)->getValue();
						$USER_ACCESS_LOGIN = $worksheet->getCellByColumnAndRow(17, $row)->getValue();
						$NE_TYPE = $worksheet->getCellByColumnAndRow(18, $row)->getValue();
						$AREA_NE = $worksheet->getCellByColumnAndRow(19, $row)->getValue();
						$ADDITIONAL_INFORMATION = $worksheet->getCellByColumnAndRow(20, $row)->getValue();
						$ACCESS_LEVEL = $worksheet->getCellByColumnAndRow(21, $row)->getValue();
						$ROLEKEYS = $worksheet->getCellByColumnAndRow(22, $row)->getValue();
						$RESPONSIBLE_APPROVAL = $worksheet->getCellByColumnAndRow(23, $row)->getValue();
						$DETAILED_LOCATION = $worksheet->getCellByColumnAndRow(24, $row)->getValue();
						$NE_NAME_IMPACTED_LIST_LIST = $worksheet->getCellByColumnAndRow(25, $row)->getValue();
						$NE_IMPACT_DESCRIPTION = $worksheet->getCellByColumnAndRow(26, $row)->getValue();
						$NE_OUTAGE_DURATION_MIN = $worksheet->getCellByColumnAndRow(27, $row)->getValue();
						$AFFECTED_SERVICE_DESCRIPTION = $worksheet->getCellByColumnAndRow(28, $row)->getValue();
						$SEVICE_OUTAGE_DURATION_MIN = $worksheet->getCellByColumnAndRow(29, $row)->getValue();
						$NE_ID_PRIMARY = $worksheet->getCellByColumnAndRow(30, $row)->getValue();
						$NE_NAME_PRIMARY = $worksheet->getCellByColumnAndRow(31, $row)->getValue();
						$BTS_NAME = $worksheet->getCellByColumnAndRow(32, $row)->getValue();
						$FREQUENCY = $worksheet->getCellByColumnAndRow(33, $row)->getValue();
						$AFFECTED_SERVICE = $worksheet->getCellByColumnAndRow(34, $row)->getValue();
						$PROVINSI = $worksheet->getCellByColumnAndRow(35, $row)->getValue();
						$KELURAHAN = $worksheet->getCellByColumnAndRow(36, $row)->getValue();
						$KECAMATAN = $worksheet->getCellByColumnAndRow(37, $row)->getValue();
						$CLUSTER = $worksheet->getCellByColumnAndRow(38, $row)->getValue();
						$CITY = $worksheet->getCellByColumnAndRow(39, $row)->getValue();

						$data = array(
							'CELL_NAME'	=> $CELL_NAME,
							'VENDOR'    => $VENDOR,
							'REGIONAL'  => $REGIONAL,
							'LAC'       => $LAC,
							'CI'        => $CI,
							'NEW_LAC'   => $NEW_LAC,
							'NEW_CI'    => $NEW_CI,
							'SITE_ID'   => $SITE_ID,
							'NE_ID'     => $NE_ID,
							'BAND'			=> $BAND,
							'DESCRIPTION_OF_CHANGE' => $DESCRIPTION_OF_CHANGE,
							'REASON_FOR_CHANGE'			=> $REASON_FOR_CHANGE,
							'EXECUTOR_PIC_COMPANY'	=> $EXECUTOR_PIC_COMPANY,
							'EXECUTOR_PIC_NAME'			=> $EXECUTOR_PIC_NAME,
							'EXECUTOR_PIC_PHONE'		=> $EXECUTOR_PIC_PHONE,
							'EXECUTOR_PIC_EMAIL'		=> $EXECUTOR_PIC_EMAIL,
							'NETWORLELEMENT_NAME'		=> $NETWORLELEMENT_NAME,
							'USER_ACCESS_LOGIN'			=> $USER_ACCESS_LOGIN,
							'NE_TYPE'								=> $NE_TYPE,
							'AREA_NE'								=> $AREA_NE,
							'ADDITIONAL_INFORMATION'=> $ADDITIONAL_INFORMATION,
							'ACCESS_LEVEL'					=> $ACCESS_LEVEL,
							'ROLEKEYS'							=> $ROLEKEYS,
							'RESPONSIBLE_APPROVAL'	=> $RESPONSIBLE_APPROVAL,
							'DETAILED_LOCATION'			=> $DETAILED_LOCATION,
							'NE_NAME_IMPACTED_LIST_LIST'	=> $NE_NAME_IMPACTED_LIST_LIST,
							'NE_IMPACT_DESCRIPTION'	=> $NE_IMPACT_DESCRIPTION,
							'NE_OUTAGE_DURATION_MIN'=> $NE_OUTAGE_DURATION_MIN,
							'AFFECTED_SERVICE_DESCRIPTION' => $AFFECTED_SERVICE_DESCRIPTION,
							'SEVICE_OUTAGE_DURATION_MIN'	 => $SEVICE_OUTAGE_DURATION_MIN,
							'NE_ID_PRIMARY'					=> $NE_ID_PRIMARY,
							'NE_NAME_PRIMARY'				=> $NE_NAME_PRIMARY,
							'BTS_NAME'							=> $BTS_NAME,
							'FREQUENCY'							=> $FREQUENCY,
							'AFFECTED_SERVICE'			=> $AFFECTED_SERVICE,
							'PROVINSI'							=> $PROVINSI,
							'KELURAHAN'							=> $KELURAHAN,
							'KECAMATAN'							=> $KECAMATAN,
							'CLUSTER'								=> $CLUSTER,
							'CITY'									=> $CITY
						);

						$datas = array(
							'CELL_NAME'		=> $CELL_NAME,
							'LAC/TAC_new' => $NEW_LAC,
							'CI_new'      => $NEW_CI,
						);

					}elseif($this->input->post('band') == "3G") {
						$CELL_NAME = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
						$VENDOR = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
						$REGIONAL = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
						$LAC = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
						$CI = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
						$NEW_LAC = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
						$NEW_CI = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
						$SITE_ID = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
						$NE_ID = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
						$BAND = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
						$DESCRIPTION_OF_CHANGE = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
						$REASON_FOR_CHANGE = $worksheet->getCellByColumnAndRow(11, $row)->getValue();
						$EXECUTOR_PIC_COMPANY = $worksheet->getCellByColumnAndRow(12, $row)->getValue();
						$EXECUTOR_PIC_NAME = $worksheet->getCellByColumnAndRow(13, $row)->getValue();
						$EXECUTOR_PIC_PHONE = $worksheet->getCellByColumnAndRow(14, $row)->getValue();
						$EXECUTOR_PIC_EMAIL = $worksheet->getCellByColumnAndRow(15, $row)->getValue();
						$NETWORLELEMENT_NAME = $worksheet->getCellByColumnAndRow(16, $row)->getValue();
						$USER_ACCESS_LOGIN = $worksheet->getCellByColumnAndRow(17, $row)->getValue();
						$NE_TYPE = $worksheet->getCellByColumnAndRow(18, $row)->getValue();
						$AREA_NE = $worksheet->getCellByColumnAndRow(19, $row)->getValue();
						$ADDITIONAL_INFORMATION = $worksheet->getCellByColumnAndRow(20, $row)->getValue();
						$ACCESS_LEVEL = $worksheet->getCellByColumnAndRow(21, $row)->getValue();
						$ROLEKEYS = $worksheet->getCellByColumnAndRow(22, $row)->getValue();
						$RESPONSIBLE_APPROVAL = $worksheet->getCellByColumnAndRow(23, $row)->getValue();
						$DETAILED_LOCATION = $worksheet->getCellByColumnAndRow(24, $row)->getValue();
						$NE_NAME_IMPACTED_LIST_LIST = $worksheet->getCellByColumnAndRow(25, $row)->getValue();
						$NE_IMPACT_DESCRIPTION = $worksheet->getCellByColumnAndRow(26, $row)->getValue();
						$NE_OUTAGE_DURATION_MIN = $worksheet->getCellByColumnAndRow(27, $row)->getValue();
						$AFFECTED_SERVICE_DESCRIPTION = $worksheet->getCellByColumnAndRow(28, $row)->getValue();
						$SEVICE_OUTAGE_DURATION_MIN = $worksheet->getCellByColumnAndRow(29, $row)->getValue();
						$NE_ID_PRIMARY = $worksheet->getCellByColumnAndRow(30, $row)->getValue();
						$NE_NAME_PRIMARY = $worksheet->getCellByColumnAndRow(31, $row)->getValue();
						$BTS_NAME = $worksheet->getCellByColumnAndRow(32, $row)->getValue();
						$FREQUENCY = $worksheet->getCellByColumnAndRow(33, $row)->getValue();
						$AFFECTED_SERVICE = $worksheet->getCellByColumnAndRow(34, $row)->getValue();
						$PROVINSI = $worksheet->getCellByColumnAndRow(35, $row)->getValue();
						$KELURAHAN = $worksheet->getCellByColumnAndRow(36, $row)->getValue();
						$KECAMATAN = $worksheet->getCellByColumnAndRow(37, $row)->getValue();
						$CLUSTER = $worksheet->getCellByColumnAndRow(38, $row)->getValue();
						$CITY = $worksheet->getCellByColumnAndRow(39, $row)->getValue();

						$data = array(
							'CELL_NAME'	=> $CELL_NAME,
							'VENDOR'    => $VENDOR,
							'REGIONAL'  => $REGIONAL,
							'LAC'       => $LAC,
							'CI'        => $CI,
							'NEW_LAC'   => $NEW_LAC,
							'NEW_CI'    => $NEW_CI,
							'SITE_ID'   => $SITE_ID,
							'NE_ID'     => $NE_ID,
							'BAND'			=> $BAND,
							'DESCRIPTION_OF_CHANGE' => $DESCRIPTION_OF_CHANGE,
							'REASON_FOR_CHANGE'			=> $REASON_FOR_CHANGE,
							'EXECUTOR_PIC_COMPANY'	=> $EXECUTOR_PIC_COMPANY,
							'EXECUTOR_PIC_NAME'			=> $EXECUTOR_PIC_NAME,
							'EXECUTOR_PIC_PHONE'		=> $EXECUTOR_PIC_PHONE,
							'EXECUTOR_PIC_EMAIL'		=> $EXECUTOR_PIC_EMAIL,
							'NETWORLELEMENT_NAME'		=> $NETWORLELEMENT_NAME,
							'USER_ACCESS_LOGIN'			=> $USER_ACCESS_LOGIN,
							'NE_TYPE'								=> $NE_TYPE,
							'AREA_NE'								=> $AREA_NE,
							'ADDITIONAL_INFORMATION'=> $ADDITIONAL_INFORMATION,
							'ACCESS_LEVEL'					=> $ACCESS_LEVEL,
							'ROLEKEYS'							=> $ROLEKEYS,
							'RESPONSIBLE_APPROVAL'	=> $RESPONSIBLE_APPROVAL,
							'DETAILED_LOCATION'			=> $DETAILED_LOCATION,
							'NE_NAME_IMPACTED_LIST_LIST'	=> $NE_NAME_IMPACTED_LIST_LIST,
							'NE_IMPACT_DESCRIPTION'	=> $NE_IMPACT_DESCRIPTION,
							'NE_OUTAGE_DURATION_MIN'=> $NE_OUTAGE_DURATION_MIN,
							'AFFECTED_SERVICE_DESCRIPTION' => $AFFECTED_SERVICE_DESCRIPTION,
							'SEVICE_OUTAGE_DURATION_MIN'	 => $SEVICE_OUTAGE_DURATION_MIN,
							'NE_ID_PRIMARY'					=> $NE_ID_PRIMARY,
							'NE_NAME_PRIMARY'				=> $NE_NAME_PRIMARY,
							'BTS_NAME'							=> $BTS_NAME,
							'FREQUENCY'							=> $FREQUENCY,
							'AFFECTED_SERVICE'			=> $AFFECTED_SERVICE,
							'PROVINSI'							=> $PROVINSI,
							'KELURAHAN'							=> $KELURAHAN,
							'KECAMATAN'							=> $KECAMATAN,
							'CLUSTER'								=> $CLUSTER,
							'CITY'									=> $CITY
						);

						$datas = array(
							'CELL_NAME'		=> $CELL_NAME,
							'LAC/TAC_new' => $NEW_LAC,
							'CI_new'      => $NEW_CI,
						);

					}elseif($this->input->post('band') == "4G"){
						$CELL_NAME = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
						$VENDOR = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
						$REGIONAL = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
						$TAC = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
						$CI = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
						$NEW_TAC = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
						$NEW_CI = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
						$SITE_ID = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
						$NE_ID = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
						$BAND = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
						$DESCRIPTION_OF_CHANGE = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
						$REASON_FOR_CHANGE = $worksheet->getCellByColumnAndRow(11, $row)->getValue();
						$EXECUTOR_PIC_COMPANY = $worksheet->getCellByColumnAndRow(12, $row)->getValue();
						$EXECUTOR_PIC_NAME = $worksheet->getCellByColumnAndRow(13, $row)->getValue();
						$EXECUTOR_PIC_PHONE = $worksheet->getCellByColumnAndRow(14, $row)->getValue();
						$EXECUTOR_PIC_EMAIL = $worksheet->getCellByColumnAndRow(15, $row)->getValue();
						$NETWORLELEMENT_NAME = $worksheet->getCellByColumnAndRow(16, $row)->getValue();
						$USER_ACCESS_LOGIN = $worksheet->getCellByColumnAndRow(17, $row)->getValue();
						$NE_TYPE = $worksheet->getCellByColumnAndRow(18, $row)->getValue();
						$AREA_NE = $worksheet->getCellByColumnAndRow(19, $row)->getValue();
						$ADDITIONAL_INFORMATION = $worksheet->getCellByColumnAndRow(20, $row)->getValue();
						$ACCESS_LEVEL = $worksheet->getCellByColumnAndRow(21, $row)->getValue();
						$ROLEKEYS = $worksheet->getCellByColumnAndRow(22, $row)->getValue();
						$RESPONSIBLE_APPROVAL = $worksheet->getCellByColumnAndRow(23, $row)->getValue();
						$DETAILED_LOCATION = $worksheet->getCellByColumnAndRow(24, $row)->getValue();
						$NE_NAME_IMPACTED_LIST_LIST = $worksheet->getCellByColumnAndRow(25, $row)->getValue();
						$NE_IMPACT_DESCRIPTION = $worksheet->getCellByColumnAndRow(26, $row)->getValue();
						$NE_OUTAGE_DURATION_MIN = $worksheet->getCellByColumnAndRow(27, $row)->getValue();
						$AFFECTED_SERVICE_DESCRIPTION = $worksheet->getCellByColumnAndRow(28, $row)->getValue();
						$SEVICE_OUTAGE_DURATION_MIN = $worksheet->getCellByColumnAndRow(29, $row)->getValue();
						$NE_ID_PRIMARY = $worksheet->getCellByColumnAndRow(30, $row)->getValue();
						$NE_NAME_PRIMARY = $worksheet->getCellByColumnAndRow(31, $row)->getValue();
						$BTS_NAME = $worksheet->getCellByColumnAndRow(32, $row)->getValue();
						$FREQUENCY = $worksheet->getCellByColumnAndRow(33, $row)->getValue();
						$AFFECTED_SERVICE = $worksheet->getCellByColumnAndRow(34, $row)->getValue();
						$PROVINSI = $worksheet->getCellByColumnAndRow(35, $row)->getValue();
						$KELURAHAN = $worksheet->getCellByColumnAndRow(36, $row)->getValue();
						$KECAMATAN = $worksheet->getCellByColumnAndRow(37, $row)->getValue();
						$CLUSTER = $worksheet->getCellByColumnAndRow(38, $row)->getValue();
						$CITY = $worksheet->getCellByColumnAndRow(39, $row)->getValue();

						$data = array(
							'CELL_NAME'	=> $CELL_NAME,
							'VENDOR'    => $VENDOR,
							'REGIONAL'  => $REGIONAL,
							'TAC'       => $TAC,
							'CI'        => $CI,
							'NEW_TAC'   => $NEW_TAC,
							'NEW_CI'    => $NEW_CI,
							'SITE_ID'   => $SITE_ID,
							'NE_ID'     => $NE_ID,
							'BAND'			=> $BAND,
							'DESCRIPTION_OF_CHANGE' => $DESCRIPTION_OF_CHANGE,
							'REASON_FOR_CHANGE'			=> $REASON_FOR_CHANGE,
							'EXECUTOR_PIC_COMPANY'	=> $EXECUTOR_PIC_COMPANY,
							'EXECUTOR_PIC_NAME'			=> $EXECUTOR_PIC_NAME,
							'EXECUTOR_PIC_PHONE'		=> $EXECUTOR_PIC_PHONE,
							'EXECUTOR_PIC_EMAIL'		=> $EXECUTOR_PIC_EMAIL,
							'NETWORLELEMENT_NAME'		=> $NETWORLELEMENT_NAME,
							'USER_ACCESS_LOGIN'			=> $USER_ACCESS_LOGIN,
							'NE_TYPE'								=> $NE_TYPE,
							'AREA_NE'								=> $AREA_NE,
							'ADDITIONAL_INFORMATION'=> $ADDITIONAL_INFORMATION,
							'ACCESS_LEVEL'					=> $ACCESS_LEVEL,
							'ROLEKEYS'							=> $ROLEKEYS,
							'RESPONSIBLE_APPROVAL'	=> $RESPONSIBLE_APPROVAL,
							'DETAILED_LOCATION'			=> $DETAILED_LOCATION,
							'NE_NAME_IMPACTED_LIST_LIST'	=> $NE_NAME_IMPACTED_LIST_LIST,
							'NE_IMPACT_DESCRIPTION'	=> $NE_IMPACT_DESCRIPTION,
							'NE_OUTAGE_DURATION_MIN'=> $NE_OUTAGE_DURATION_MIN,
							'AFFECTED_SERVICE_DESCRIPTION' => $AFFECTED_SERVICE_DESCRIPTION,
							'SEVICE_OUTAGE_DURATION_MIN'	 => $SEVICE_OUTAGE_DURATION_MIN,
							'NE_ID_PRIMARY'					=> $NE_ID_PRIMARY,
							'NE_NAME_PRIMARY'				=> $NE_NAME_PRIMARY,
							'BTS_NAME'							=> $BTS_NAME,
							'FREQUENCY'							=> $FREQUENCY,
							'AFFECTED_SERVICE'			=> $AFFECTED_SERVICE,
							'PROVINSI'							=> $PROVINSI,
							'KELURAHAN'							=> $KELURAHAN,
							'KECAMATAN'							=> $KECAMATAN,
							'CLUSTER'								=> $CLUSTER,
							'CITY'									=> $CITY
						);

						$datas = array(
							'CELL_NAME'		=> $CELL_NAME,
							'LAC/TAC_new' => $NEW_TAC,
							'CI_new'      => $NEW_CI,
						);
					}
					$import = $this->curl->simple_put($this->API_Nodin.'/NodinController', $data, array(CURLOPT_BUFFERSIZE => 10));
					$import = $this->curl->simple_post($this->API_Nodin.'/NodinController', $datas, array(CURLOPT_BUFFERSIZE => 10));
				}
			}

			print_r($data);

			if($import){
				// $this->session->set_flashdata('hasil','Import Excle File Success');
			} else{
				// $this->session->set_flashdata('hasil','Import Excle File Fail');
			}

			redirect('NodinController/index_nodin');
		}
	}

	function edit(){
		if(isset($posted['submit'])){
			$data = array(
				'infra_status'       => $this->input->post('infra_status'),
				'swoid'				 => $this->input->post('swoid')
			);
			$update =  $this->curl->simple_put($this->API.'/DafinciController', $data, array(CURLOPT_BUFFERSIZE => 10));
			if($update)
			{
				$this->session->set_flashdata('hasil','Update Data Berhasil');
			}else
			{
				$this->session->set_flashdata('hasil','Update Data Gagal');
			}
			redirect('NodinController');
		}else{
			$params = array('swoid'=>  $this->uri->segment(3));
			$data['datanodim'] = json_decode($this->curl->simple_get($this->API.'/NodinController',$params));
			$this->load->view('kontak/edit',$data);
		}
	}

	function pdf(){
		$this->load->library('PdfGenerator');
		$data[]= array(
			'siteid'            => $siteid,
			'ne_id'             => $ne_id,
			'sector_id'         => $sector_id,
			'site_name'         => $site_name,
			'oss_name'          => $oss_name,
			'kabupaten'         => $kabupaten,
			'lac'               => $lac,
			'cell_name'         => $cell_name,
			'sac'               => $sac,
			'scrambling_code'   => $scrambling_code,
			'rnc'               => $rnc,
			'rnc_id'            => $rnc_id,
			'rac'               => $rac,
			'ura_id'            => $ura_id,
			'mscs_name'         => $mscs_name,
			'mscs_spc'          => $mscs_spc,
			'mgw_name'          => $mgw_name,
			'mgw_spc'           => $mgw_spc,
			'locno'             => $locno,
			'poc_pstn'          => $poc_pstn,
			'time_zone'         => $time_zone,
			'sow'               => $sow,
			'longitude'         => $longitude,
			'latitude'          => $latitude
		);
	}

	function auto_neid()
	{
		$neid = json_decode($this->curl->simple_get($this->API_AutoNEID."?ne_id=".$_GET['term']));

		$return_neid = [];
		foreach ($neid as $row) {
			$array = [
				'value' => $row->NE_ID,
				'label' => $row->NE_ID,
				'id' => $row->NE_ID
			];
			array_push($return_neid, $array);

		}

		// set text compatible IE7, IE8
		header('Content-type: text/plain');
		// set json non IE
		header('Content-type: application/json');

		echo json_encode($return_neid);
	}
}

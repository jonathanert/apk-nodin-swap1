<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Nusoap_library
{

   function __construct(){
     require_once('lib/nusoap.php');
     require_once('lib/class.wsdlcache.php');
   }

   function soaprequest($api_url, $api_username, $api_password, $params){
    ini_set('max_execution_time', 9000);
    ini_set('default_socket_timeout', 9000);
    if ($api_url != '' && $params != ''){
        $client = new nusoap_client($api_url, false);
        $client->soap_defencoding = 'utf-8';
        $client->useHTTPPersistentConnection();
        $client->setCredentials($api_username,$api_password);

        $msg = $client->serializeEnvelope($params);
        $error = $client->getError();
        if ($error){
            echo "\nSOAP Error\n".$error."\n";
            return false;
        }
        else{
            $result=$client->send($params, $api_url);
            if ($client->fault) {
             echo '<h2>Fault</h2><pre>';
             print_r($result);
             echo '</pre>';
            } else {
             // Check for errors
             $err = $client->getError();
             if ($err) {
              // Display the error
              echo '<h2>Error</h2><pre>' . $err . '</pre>';
             } else {
              // Display the result
              echo '<h2>Result</h2><pre>';
              echo $result['Change_ID'];
              // print_r($result);
              echo '</pre>';

              return $result['Change_ID'];
             }
            }
            // $result = $client->call($service, $params);
            // if ($client->fault)
            // {
            //     print_r($result);
            //     return false;
            // }
            // else
            // {
            //     $result_arr = json_decode($result, true);
            //     $return_array = $result_arr['result'];
            //     return $return_array;
            // }
        }
    }
  }
}

 ?>

<!DOCTYPE html>
<html lang="en" class="no-js">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>NEISA | CREATE SWAP</title>
    <meta name="description" content="A sidebar menu as seen on the Google Nexus 7 website" />
    <meta name="keywords" content="google nexus 7 menu, css transitions, sidebar, side menu, slide out menu" />
    <meta name="author" content="Codrops" />
    <link rel="shortcut icon" href="../favicon.ico">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/plugins/google/css/normalize.css');;?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/plugins/google/css/demos.css');?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/plugins/google/css/component.css');?>" />
    <link rel="stylesheet" href="<?php echo base_url('asset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('asset/plugins/bootstrap/css/bootstrap.min.css')?>">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"> -->
    <!-- <link rel="stylesheet" href="<?php echo base_url('asset/plugins/font-awesome/css/font-awesome.min.css');?>"> -->
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url('asset/dist/css/adminlte.min.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('asset/css/style.css')?>">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo base_url('asset/plugins/iCheck/flat/blue.css')?>">

    <!-- *** START TAMBAH IMPORT FONTAWESOME IYON *** -->
    <!-- <link rel="stylesheet" type="text/css" href="./asset/plugins/fontawesome-free-5.6.3-web/css/fontawesome.css">
    <link rel="stylesheet" type="text/css" href="./asset/plugins/fontawesome-free-5.6.3-web/css/solid.css">
    <link rel="stylesheet" type="text/css" href="./asset/plugins/fontawesome-free-5.6.3-web/css/brands.css"> -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/plugins/fontawesome-free-5.6.3-web/css/fontawesome.css');?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/plugins/fontawesome-free-5.6.3-web/css/solid.css');?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/plugins/fontawesome-free-5.6.3-web/css/brands.css');?>">
    <!-- *** END TAMBAH IMPORT FONTAWESOME IYON *** -->

    <style media="screen">
      .nav-button{
      color: #007bff;
      margin-left: -3px;
      }

      .nav-button:hover {
      color: #0056b3 !important;
      }

                  /* ** START TAMBAH CSS IYON** */
    .dropClass {

visibility: visible;
opacity: 1;
height: auto;
padding: 0.5rem 1rem;
background-color: rgba(255,255,255,.3);
margin-bottom: 1.5%;
border-radius: 3%;

}

#dropIcon {
transform: rotate(90deg);
}

.dropClass a {
color: black;
padding: 12px 16px;
display: block;
}

.dropClass a:hover {background-color: rgba(255,255,255,.1);}
/* .show {
visibility: visible;
opacity: 1;
height: auto;
padding: 0.5rem 1rem;
background-color: rgba(255,255,255,.3);
margin-bottom: 1.5%;
border-radius: 3%;
}

.putar {
transform: rotate(90deg);
transition: all .5s ease;
} */

.brand-image {
line-height: .8;
max-height: 53px;
width: auto;
margin-left: 0.7rem;
margin-right: .5rem;
margin-top: -3px;
float: none;
opacity: .9;
}

.backgroundImg {
width: auto;
height: 100%;
opacity: 1;
position: absolute;
}

.backgroundImg2 {
position: fixed;
width: 100%;
max-height: 56px;
margin-left: -2%;
opacity: 1;
}

.nav-item:hover {
background-color: rgba(255,255,255,.3);
border-radius: 5%;
transition: all .2s ease;
}

.active {
background-color: rgba(243, 255, 226, .8) !important;
color: #343a40 !important;
font-weight: 600;
}

.berisik {
min-height:500px !important
}
/* ** END TAMBAH IYON** */


    </style>
    <script src="<?php echo base_url('asset/plugins/google/js/modernizr.custom.js');?>">
    </script>

  </head>

 <body class="hold-transition sidebar-mini" style="background: #f4f6f9; color: white;">
    <nav class="main-header navbar navbar-expand bg-dark" style="margin-left: 250px; position:fixed; width:100%;">
    <img src="<?php echo base_url('asset/dist/img/wall5.jpg');?>" class="backgroundImg2" style="position: fixed;
    width: 100%;">
    <!-- <img src="./asset/dist/img/wall5.jpg" class="backgroundImg2" style="position: fixed;
    width: 100%;"> -->
      <!-- Left navbar links -->
      <ul class="navbar-nav" style="z-index: 999;">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars nav-button" style="color:white;"></i></a>
        </li>
        <li class="navbar-brand" style="color:white;margin-left: 10%;">NEISA | CREATE SWAP</li>
        <li class="nav-item">
          <a class="nav-link btn btn-lg" href="http://10.54.36.49/landingPage/" onclick="sessionStorage.clear();" style="
                  color: #343a40 !important;
                  background-color: #fff;
                  position: fixed;
                  font-size: 10px;
                  right: 1%;
                  height: auto;
                  box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
                  text-transform: uppercase;
                  font-family: Roboto;
                  padding: 1%;"><i class="fa fa-sign-out-alt"></i> Log Out</a>
        </li>
      </ul>
    </nav>

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4 berisik" >
    <img src="<?php echo base_url('asset/dist/img/wall3.jpg');?>" class="backgroundImg">
      <!-- <img src="./asset/dist/img/wall3.jpg" class="backgroundImg">    -->
      <!-- Brand Logo -->
      <a href="#" class="brand-link">
        <img src="<?php echo base_url('asset/dist/img/telkomsel.png');?>" alt="AdminLTE Logo" class="brand-image"
             style="opacity: .8; float:none; widht:200px; line-height:.8; max-height:53px;margin-left:0.7rem;margin-right:.5rem;margin-top:-3px">
      </a>

      <!-- Sidebar -->
      <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <!-- Sidebar Menu -->
        <nav class="mt-2">

        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <li class="nav-item">
                            <a href="http://10.54.36.49/dashboard-bts-on-air/public/" class="nav-link" style="color: #fff;padding: 0.5rem 1rem !important;">
                            <i class="nav-icon fa fa-home"></i>
                                <p style="margin-left: 3px;">Dashboard</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="http://10.54.36.49/dashboard-license" class="nav-link aa " style="color: #fff;padding: 0.5rem 1rem !important;">
                            <i class="nav-icon fa fa-newspaper"></i>
                                <p style="margin-left: 3px;">License</p>
                            </a>
                        </li>
                        <!-- <li class="nav-item">
                            <a href="http://10.54.36.49/btsonair" class="nav-link " style="color: #fff;padding: 0.5rem 1rem !important;">
                            <i class="nav-icon fa fa-broadcast-tower"></i>
                                <p style="margin-left: 3px;">BTS Status</p>
                            </a>
                        </li> -->
        
                        <li class="nav-item" style="cursor: pointer;">
                            <a onclick="dropDead()" class="nav-link aa dropbtn active" style="color: #fff;padding: 0.5rem 1rem !important;">
                                <i class="nav-icon fa fa-angle-right" id="dropIcon"></i>
                                <p style="margin-left: 3px;">Create</p>
                            </a>
                        </li>
        
                        
                        <div class="dropClass" id="dropId">
                        <li class="nav-item">
                            <a href="http://10.54.36.49/apk-nodin/index.php/NodinController" class="nav-link aa" style="color: #fff;padding: 0.5rem 1rem !important;">
                                <i class="nav-icon fa fa-list-alt"></i>
                                <p style="margin-left: 3px;">Create Integrasi</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="http://10.54.36.49/apk-nodin-stylo/index.php/NodinController" class="nav-link aa " style="color: #fff;padding: 0.5rem 1rem !important;">
                                <i class="nav-icon fa fa-list-alt"></i>
                                <p style="margin-left: 3px;">Create Rehoming</p>
                            </a>
                        </li>
                        <li class="nav-item">
                          <a href="http://10.54.36.49/apk-nodin-dismantle/index.php/NodinController" class="nav-link aa " style="color: #fff;padding: 0.5rem 1rem !important;">
                            <i class="nav-icon fa fa-list-alt"></i>
                              <p style="margin-left: 3px;">Create Dismantle</p>
                          </a>
                      </li>
                      <li class="nav-item">
                          <a href="http://10.54.36.49/apk-nodin-relocation/index.php/NodinController" class="nav-link aa " style="color: #fff;padding: 0.5rem 1rem !important;">
                            <i class="nav-icon fa fa-list-alt"></i>
                              <p style="margin-left: 3px;">Create Relocation</p>
                          </a>
                      </li>
                      <li class="nav-item">
                        <a href="http://10.54.36.49/apk-nodin-swap/index.php/NodinController" class="nav-link aa active" style="color: #fff;padding: 0.5rem 1rem !important;">
                            <i class="nav-icon fa fa-list-alt"></i>
                            <p style="margin-left: 3px;">Create Swap</p>
                        </a>
                    </li>
                </div>
                        <li class="nav-item">
                            <a href="http://10.54.36.49/change-front-2/public/" class="nav-link aa" style="color: #fff;padding: 0.5rem 1rem !important;">
                            <i class="nav-icon fa fa-project-diagram"></i>
                                <p style="margin-left: 3px;">Process Tracking</p>
                            </a>
                        </li>
                        <li class="nav-item">
                          <a href="http://10.54.36.49/tableList" class="nav-link aa" style="color: #fff;padding: 0.5rem 1rem !important;">
                          <i class="nav-icon fa fa-table"></i>
                              <p style="margin-left: 3px;">Nodin & MoM Report</p>
                          </a>
                      </li>
                      <li class="nav-item">
                        <a href="http://10.54.36.49/apk-report/index.php/ReportController" class="nav-link aa" style="color: #fff;padding: 0.5rem 1rem !important;">
                            <i class="nav-icon fa fa-book"></i>
                            <p style="margin-left: 3px;">Report Remedy</p>
                        </a>
                    </li>
                  </ul>
        </nav>
        <!-- /.sidebar-menu -->
      </div>
      <!-- /.sidebar -->
    </aside>
        <!-- /.col -->
    </div>
      <!-- /.row -->
    <div class="content-wrapper" style=" margin-left: 250px; background: #f4f6f9; margin-top: 50px;">
      <div class="card bg-white">
        <div class="row">
        <div class="col-lg-12">
          <!-- /.breadcrumb -->
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="<?php echo base_url('index.php/NodinController/index?token=');?>">List Data</a></li>
              <li class="breadcrumb-item"><a href="<?php echo base_url('index.php/NodinController/index_nodin?token=');?>">Update Data Nodin</a></li>
              <li class="breadcrumb-item active" aria-current="page">List Data Nodin</li>
            </ol>
          </nav>
          <?php
          if(isset($_GET['success'])) {
            echo '
            <div class="alert alert-success" role="alert">
            '.$_GET["success"].'
            </div>
            ';
          }
          ?>
          <div class="col-md-12 mailbox-controls" style="width: 100%; margin:5px;">
            <div class="row">
                <div class="col-md-9">
                </div>
                <?php echo form_open_multipart("NodinController/index_listnodin");?>
                <div class="input-group input-group-sm">
                  <input type="text" class="form-control" name="search" placeholder="Search Data Based On Nodin ID" style="width: 220px; align-content: right;">
                  <div class="input-group-append">
                    <input type="submit" name="submit" value="search">
                  </div>
                  </div>
                <?php echo form_close(); ?>
              </div>
            </div>

           <div class="table-responsive mailbox-messages" style="width: 100%; font-size: 12px; margin:5px;">
              <?php echo form_open_multipart('NodinController/insert');?>
                <table id="example2" class="table table-bordered table-hover">
                  <tbody>
                   <tr>
                      <th>No</th>
                      <th>Nodin ID</th>
                      <th>CRQ</th>
                      <th>Input Date</th>
                      <th>Action</th>
                    </tr>
                    <?php
                    // echo $this->session->flashdata('hasil');
                    $CRQ_ALL = "";
                    foreach ($datanodin as $nodim) {
                      foreach ($crqs as $crq) {
                        if($nodim->NODIN_ID == $crq->NODIN_ID) {
                          $CRQ_ALL .= 'CELL NAME : '.$crq->CELL_NAME.' - CRQ : '.$crq->CRQ.'|';
                        }
                      }
                    }

                    $i = 1;
                    foreach ($datanodin as $nodim){
                      if($nodim->NODIN_ID != "") {
                        echo "
                          <tr>
                            <td>".$i."</td>
                            <td>$nodim->NODIN_ID</td>
                            <td>".str_replace('|','<br>',$CRQ_ALL)."</td>
                            <td>$nodim->INPUT_DATE</td>
                            <td>
                              <a target='_blank' class='btn btn-success' href=".base_url("index.php/NodinController/index_export?nodin_id=".$nodim->NODIN_ID.'&token=').">Export Nodin</a>
                            </td>
                          </tr>
                        ";
                      }

                        $i++;
                      }

                    ?>
                  </tbody>
                </table>
                <div class="col-md-12">
                  <div id="pagination" class="scrollmenu">
                    <ul class="tsc_pagination">

                    <!-- Show pagination links -->
                    <?php foreach ($links as $link) {
                    echo "<li> ". $link." </li>";
                    } ?>
                    </ul>
                  </div>
                </div>
                <!-- /.table -->
              </div>
                 <div class="card-footer" style="list">
                  <div class="float-right">
                    <?php
                      echo form_submit('submit','Submit',array('class' => 'btn btn-primary'));
                    ?>
                  </div>
                  <button type="reset" class="btn btn-default"><i class="fa fa-times"></i> Discard</button>
                </div>
                <?php echo form_close(); ?>
                <!-- /.mail-box-messages -->
        </div>
        </div>
      </div>
    <!-- /.content -->
    </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer sticky-bottom" style="margin-left: 250px; background: white; color: black;">
  <strong style="font-size: 12px">Copyright &copy; 2018 <a style="color:white;">Telkomsel</a>.</strong>
  <div class="float-right d-none d-sm-inline-block">
      </div>
  </footer>
  </div><!-- /container -->
  <script src="<?php echo base_url('asset/plugins/google/js/classie.js');?>"></script>
  <script src="<?php echo base_url('asset/plugins/google/js/gnmenu.js');?>"></script>
  <!-- jQuery -->
  <script type="text/javascript" src="<?php echo base_url('/asset/plugins/jquery/jquery.min.js'); ?>"></script>
  <script src="<?php echo base_url('asset/plugins/jQueryUI/jquery-ui.min.js'); ?>"></script>
  <!-- Bootstrap 4 -->
  <script type="text/javascript" src="<?php echo base_url('asset/plugins/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
  <!-- Slimscroll -->
  <script type="text/javascript" src="<?php echo base_url('asset/plugins/slimScroll/jquery.slimscroll.min.js'); ?>"></script>
  <!-- FastClick -->
  <script type="text/javascript" src="<?php echo base_url('asset/plugins/fastclick/fastclick.js'); ?>"></script>
  <!-- AdminLTE App -->
  <script type="text/javascript" src="<?php echo base_url('asset/dist/js/adminlte.min.js'); ?>"></script>
  <!-- iCheck -->
  <script type="text/javascript" src="<?php echo base_url('asset/plugins/iCheck/icheck.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('asset/plugins/bootstrap/js/bootstrap.min.js'); ?>"></script>
  <script src="<?php echo base_url('asset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js');?>"></script>
    <script>
      new gnMenu( document.getElementById( 'gn-menu' ) );
    </script>
    <script>
    (function ($) {
    //Enable iCheck plugin for checkboxes
    //iCheck for checkbox and radio inputs
    $('.mailbox-messages input[type="checkbox"]').iCheck({
      checkboxClass: 'icheckbox_flat-blue',
      radioClass   : 'iradio_flat-blue'
    })

    //Enable check and uncheck all functionality
    $('.checkbox-toggle').click(function () {
      var clicks = $(this).data('clicks')
      if (clicks) {
        //Uncheck all checkboxes
        $('.mailbox-messages input[type=\'checkbox\']').iCheck('uncheck')
        $('.fa', this).removeClass('fa-check-square').addClass('fa-square')
      } else {
        //Check all checkboxes
        $('.mailbox-messages input[type=\'checkbox\']').iCheck('check')
        $('.fa', this).removeClass('fa-square').addClass('fa-check-square')
      }
      $(this).data('clicks', !clicks)
    })

    //Handle starring for glyphicon and font awesome
    $('.mailbox-star').click(function (e) {
      e.preventDefault()
      //detect type
      var $this = $(this).find('a > i')
      var glyph = $this.hasClass('glyphicon')
      var fa    = $this.hasClass('fa')

      //Switch states
      if (glyph) {
        $this.toggleClass('glyphicon-star')
        $this.toggleClass('glyphicon-star-empty')
      }

      if (fa) {
        $this.toggleClass('fa-star')
        $this.toggleClass('fa-star-o')
      }
    })
  })(jQuery)
</script>
  </body>
</html>

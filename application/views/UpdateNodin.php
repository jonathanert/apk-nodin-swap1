<!DOCTYPE html>
<html lang="en" class="no-js">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>NEISA | CREATE SWAP</title>
    <meta name="description" content="A sidebar menu as seen on the Google Nexus 7 website" />
    <meta name="keywords" content="google nexus 7 menu, css transitions, sidebar, side menu, slide out menu" />
    <meta name="author" content="Codrops" />
    <link rel="shortcut icon" href="../favicon.ico">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/plugins/google/css/normalize.css');;?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/plugins/google/css/demos.css');?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/plugins/google/css/component.css');?>" />
    <link rel="stylesheet" href="<?php echo base_url('asset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('asset/plugins/bootstrap/css/bootstrap.min.css')?>">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"> -->
    <!-- <link rel="stylesheet" href="<?php echo base_url('asset/plugins/font-awesome/css/font-awesome.min.css');?>"> -->
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url('asset/dist/css/adminlte.min.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('asset/css/style.css')?>">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo base_url('asset/plugins/iCheck/flat/blue.css')?>">

    <!-- *** START TAMBAH IMPORT FONTAWESOME IYON *** -->
    <!-- <link rel="stylesheet" type="text/css" href="./asset/plugins/fontawesome-free-5.6.3-web/css/fontawesome.css">
    <link rel="stylesheet" type="text/css" href="./asset/plugins/fontawesome-free-5.6.3-web/css/solid.css">
    <link rel="stylesheet" type="text/css" href="./asset/plugins/fontawesome-free-5.6.3-web/css/brands.css"> -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/plugins/fontawesome-free-5.6.3-web/css/fontawesome.css');?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/plugins/fontawesome-free-5.6.3-web/css/solid.css');?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/plugins/fontawesome-free-5.6.3-web/css/brands.css');?>">
    <!-- *** END TAMBAH IMPORT FONTAWESOME IYON *** -->

    <style media="screen">
      .nav-button{
      color: #007bff;
      margin-left: -3px;
      }

      .nav-button:hover {
      color: #0056b3 !important;
      }

                  /* ** START TAMBAH CSS IYON** */
    .dropClass {

visibility: visible;
opacity: 1;
height: auto;
padding: 0.5rem 1rem;
background-color: rgba(255,255,255,.3);
margin-bottom: 1.5%;
border-radius: 3%;

}

#dropIcon {
transform: rotate(90deg);
}

.dropClass a {
color: black;
padding: 12px 16px;
display: block;
}

.dropClass a:hover {background-color: rgba(255,255,255,.1);}
/* .show {
visibility: visible;
opacity: 1;
height: auto;
padding: 0.5rem 1rem;
background-color: rgba(255,255,255,.3);
margin-bottom: 1.5%;
border-radius: 3%;
}

.putar {
transform: rotate(90deg);
transition: all .5s ease;
} */

.brand-image {
line-height: .8;
max-height: 53px;
width: auto;
margin-left: 0.7rem;
margin-right: .5rem;
margin-top: -3px;
float: none;
opacity: .9;
}

.backgroundImg {
width: auto;
height: 100%;
opacity: 1;
position: absolute;
}

.backgroundImg2 {
position: fixed;
width: 100%;
max-height: 56px;
margin-left: -2%;
opacity: 1;
}

.nav-item:hover {
background-color: rgba(255,255,255,.3);
border-radius: 5%;
transition: all .2s ease;
}

.active {
background-color: rgba(243, 255, 226, .8) !important;
color: #343a40 !important;
font-weight: 600;
}

.berisik {
min-height:500px !important
}
/* ** END TAMBAH IYON** */


    </style>
    <script src="<?php echo base_url('asset/plugins/google/js/modernizr.custom.js');?>">
    </script>

  </head>

 <body class="hold-transition sidebar-mini" style="background: #f4f6f9; color: white;">
    <nav class="main-header navbar navbar-expand bg-dark" style="margin-left: 250px; position:fixed; width:100%;">
    <img src="<?php echo base_url('asset/dist/img/wall5.jpg');?>" class="backgroundImg2" style="position: fixed;
    width: 100%;">
    <!-- <img src="./asset/dist/img/wall5.jpg" class="backgroundImg2" style="position: fixed;
    width: 100%;"> -->
      <!-- Left navbar links -->
      <ul class="navbar-nav" style="z-index: 999;">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars nav-button" style="color:white;"></i></a>
        </li>
        <li class="navbar-brand" style="color:white;margin-left: 10%;">NEISA | CREATE SWAP</li>
        <li class="nav-item">
          <a class="nav-link btn btn-lg" href="http://10.54.36.49/landingPage/" onclick="sessionStorage.clear();" style="
                  color: #343a40 !important;
                  background-color: #fff;
                  position: fixed;
                  font-size: 10px;
                  right: 1%;
                  height: auto;
                  box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
                  text-transform: uppercase;
                  font-family: Roboto;
                  padding: 1%;"><i class="fa fa-sign-out-alt"></i> Log Out</a>
        </li>
      </ul>
    </nav>

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4 berisik" >
    <img src="<?php echo base_url('asset/dist/img/wall3.jpg');?>" class="backgroundImg">
      <!-- <img src="./asset/dist/img/wall3.jpg" class="backgroundImg">    -->
      <!-- Brand Logo -->
      <a href="#" class="brand-link">
        <img src="<?php echo base_url('asset/dist/img/telkomsel.png');?>" alt="AdminLTE Logo" class="brand-image"
             style="opacity: .8; float:none; widht:200px; line-height:.8; max-height:53px;margin-left:0.7rem;margin-right:.5rem;margin-top:-3px">
      </a>

      <!-- Sidebar -->
      <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <!-- Sidebar Menu -->
        <nav class="mt-2">

        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <li class="nav-item">
                            <a href="http://10.54.36.49/dashboard-bts-on-air/public/" class="nav-link" style="color: #fff;padding: 0.5rem 1rem !important;">
                            <i class="nav-icon fa fa-home"></i>
                                <p style="margin-left: 3px;">Dashboard</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="http://10.54.36.49/dashboard-license" class="nav-link aa " style="color: #fff;padding: 0.5rem 1rem !important;">
                            <i class="nav-icon fa fa-newspaper"></i>
                                <p style="margin-left: 3px;">License</p>
                            </a>
                        </li>
                        <!-- <li class="nav-item">
                            <a href="http://10.54.36.49/btsonair" class="nav-link " style="color: #fff;padding: 0.5rem 1rem !important;">
                            <i class="nav-icon fa fa-broadcast-tower"></i>
                                <p style="margin-left: 3px;">BTS Status</p>
                            </a>
                        </li> -->
        
                        <li class="nav-item" style="cursor: pointer;">
                            <a onclick="dropDead()" class="nav-link aa dropbtn active" style="color: #fff;padding: 0.5rem 1rem !important;">
                                <i class="nav-icon fa fa-angle-right" id="dropIcon"></i>
                                <p style="margin-left: 3px;">Create</p>
                            </a>
                        </li>
        
                        
                        <div class="dropClass" id="dropId">
                        <li class="nav-item">
                            <a href="http://10.54.36.49/apk-nodin/index.php/NodinController" class="nav-link aa" style="color: #fff;padding: 0.5rem 1rem !important;">
                                <i class="nav-icon fa fa-list-alt"></i>
                                <p style="margin-left: 3px;">Create Integrasi</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="http://10.54.36.49/apk-nodin-stylo/index.php/NodinController" class="nav-link aa " style="color: #fff;padding: 0.5rem 1rem !important;">
                                <i class="nav-icon fa fa-list-alt"></i>
                                <p style="margin-left: 3px;">Create Rehoming</p>
                            </a>
                        </li>
                        <li class="nav-item">
                          <a href="http://10.54.36.49/apk-nodin-dismantle/index.php/NodinController" class="nav-link aa " style="color: #fff;padding: 0.5rem 1rem !important;">
                            <i class="nav-icon fa fa-list-alt"></i>
                              <p style="margin-left: 3px;">Create Dismantle</p>
                          </a>
                      </li>
                      <li class="nav-item">
                          <a href="http://10.54.36.49/apk-nodin-relocation/index.php/NodinController" class="nav-link aa " style="color: #fff;padding: 0.5rem 1rem !important;">
                            <i class="nav-icon fa fa-list-alt"></i>
                              <p style="margin-left: 3px;">Create Relocation</p>
                          </a>
                      </li>
                      <li class="nav-item">
                        <a href="http://10.54.36.49/apk-nodin-swap/index.php/NodinController" class="nav-link aa active" style="color: #fff;padding: 0.5rem 1rem !important;">
                            <i class="nav-icon fa fa-list-alt"></i>
                            <p style="margin-left: 3px;">Create Swap</p>
                        </a>
                    </li>
                </div>
                        <li class="nav-item">
                            <a href="http://10.54.36.49/change-front-2/public/" class="nav-link aa" style="color: #fff;padding: 0.5rem 1rem !important;">
                            <i class="nav-icon fa fa-project-diagram"></i>
                                <p style="margin-left: 3px;">Process Tracking</p>
                            </a>
                        </li>
                        <li class="nav-item">
                          <a href="http://10.54.36.49/tableList" class="nav-link aa" style="color: #fff;padding: 0.5rem 1rem !important;">
                          <i class="nav-icon fa fa-table"></i>
                              <p style="margin-left: 3px;">Nodin & MoM Report</p>
                          </a>
                      </li>
                      <li class="nav-item">
                        <a href="http://10.54.36.49/apk-report/index.php/ReportController" class="nav-link aa" style="color: #fff;padding: 0.5rem 1rem !important;">
                            <i class="nav-icon fa fa-book"></i>
                            <p style="margin-left: 3px;">Report Remedy</p>
                        </a>
                    </li>
                  </ul>
        </nav>
        <!-- /.sidebar-menu -->
      </div>
      <!-- /.sidebar -->
    </aside>
        <!-- /.col -->
    </div>

      <!-- /.row -->
    <div class="content-wrapper" style=" margin-left: 250px; background: #f4f6f9; margin-top: 50px;">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo base_url('index.php/NodinController/index?token=');?>">List Data</a></li>
          <li class="breadcrumb-item active" aria-current="page">Update Data Nodin</li>
          <li class="breadcrumb-item"><a href="<?php echo base_url('index.php/NodinController/index_listnodin?token=');?>">List Data Nodin</a></li>
        </ol>
      </nav>
      <div class="row" style="width:100%; margin:5px;">
       <div class="card bg-white">
        <div class="col-lg-12">
          <center>
                <h3 align="center">Import Excel File For Update Data</h3>
                <form method="post" id="import_form" enctype="multipart/form-data" action="<?php echo base_url('index.php/NodinController/import'); ?>">
                  <div class="col-md-12">
                    <div class="row">
                      <div class="col-md-1 offset-lg-4">
                        <div class="input-group mb-2">
                            <select name="band" class="custom-select" id="inputGroupSelect01">
                            <option name="band" selected>Band</option>
                            <option name="band" value="2G">2G</option>
                            <option name="band" value="3G">3G</option>
                            <option name="band" value="4G">4G</option>
                            </select>
                        </div>
                      </div>
                      <div class="col-md-4" style="margin-left: -10px;">
                        <input type="file" name="file" id="file" accept=".xls, .xlsx" style="margin-left: -25px;" />
                        <input type="submit" name="import" value="Import" class="btn btn-primary"  style="margin-left: -70px;"/>
                      </div>
                    </div>
                  </div>
                </form>
          </center>

          <div class="col-md-12 mailbox-controls" style="width:100%; margin:5px;">
            <div class="row">
              <div class="col-md-4">
                <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square"></i>
                </button>
              </div>
              <div class="col-md-5 offset-md-2">
                <div class="float-right">
                  <?php echo form_open_multipart("NodinController/index_nodin");?>
                  <div class="input-group input-group-sm" style="margin:5px;">
                    <input type="text" class="form-control" name="search" placeholder="Search Data Based On Nodin ID" style="width: 220px; align-content: right;">
                    <div class="input-group-append">
                      <input type="submit" name="submit" value="search">
                    </div>
                    </div>
                  <?php echo form_close(); ?>
                </div>
              </div>
            </div>
          </div>
          <?php
          $uri = isset($_GET['ne_id']);
          $remedy = isset($_GET['remedy']);
          if($uri != NULL) {
            echo "
              <div class='alert alert-warning' role='alert'>
                NE ID Not Found
              </div>
            ";
          }
          if($remedy != NULL) {
            echo "
              <div class='alert alert-warning' role='alert'>
                Gagal upload file
              </div>
            ";
          }
          ?>
            <div class="table-responsive mailbox-messages" style="width: 100%; font-size: 12px; margin:5px;">
              <?php echo form_open_multipart('NodinController/set_nodinid',array('name' => 'theform'));?>
                <table id="example2" class="table table-bordered table-hover">
                  <tbody>
                    <tr class="bg-dark">
                      <th>Pilih</th>
                      <th>NODIN ID</th>
                      <th>CELL NAME</th>
                      <th>VENDOR</th>
                      <th>REGIONAL</th>
                      <th>LAC</th>
                      <th>CI</th>
                      <th>NEW LAC</th>
                      <th>NEW CI</th>
                      <th>SITE ID</th>
                      <th>NE ID</th>
                      <th>BAND</th>
                      <th>DESCRIPTION</th>
                      <th>SWAP</th>
                      <th>DESCRIPTION OF CHANGE</th>
                      <th>REASON FOR CHANGE</th>
                      <th>RESPONSIBLE APPROVAL</th>
                      <th>DETAILED LOCATION</th>
                      <th>NE ID PRIMARY</th>
                      <th>NE NAME PRIMARY</th>
                      <th>PROGRAM</th>
                      <th>SOW</th>
                      <th>SITE NAME</th>
                      <th>BSC NAME</th>
                      <th>BSC ID</th>
                      <th>MCC</th>
                      <th>MNC</th>
                      <th>EXECUTOR PIC COMPANY</th>
                      <th>EXECUTOR PIC NAME</th>
                      <th>EXECUTOR PIC PHONE</th>
                      <th>EXECUTOR PIC EMAIL</th>
                      <th>NETWORLELEMENT NAME</th>
                      <th>USER ACCESS LOGIN</th>
                      <th>NE TYPE</th>
                      <th>AREA NE</th>
                      <th>ADDITIONAL INFORMATION</th>
                      <th>ACCESS LEVEL</th>
                      <th>NETWORK TYPE</th>
                      <th>ROLEKEYS</th>
                      <th>AFFECTED SERVICE DESCRIPTION</th>
                      <th>SEVICE OUTAGE DURATION MIN</th>
                      <th>BTS NAME</th>
                      <th>FREQUENCY</th>
                      <th>AFFECTED SERVICE</th>
                      <th>PROVINSI</th>
                      <th>KELURAHAN</th>
                      <th>KECAMATAN</th>
                      <th>CLUSTER</th>
                      <th>CITY</th>
                     </tr>
                    <?php
                    // echo $this->session->flashdata('hasil');
                    $i = 1;
                    foreach ($datanodin as $nodim){
                      if($nodim->NEW_LAC != "" AND $nodim->NEW_CI != "" AND $nodim->NODIN_ID != "") {
                        if($nodim->BAND != "4G") {
                          echo "
                            <tr>
                              <td>
                                <center>
                                <center><i class='fa fa-check-square' style='color: green;' data-toggle='tooltip' data-placement='top' title='Nomor Nodin Telah Diberikan'></i></center>
                                </center>
                              </td>
                              <td>$nodim->NODIN_ID</td>
                              <td>$nodim->CELL_NAME</td>
                              <td>$nodim->VENDOR</td>
                              <td>$nodim->REGIONAL</td>
                              <td>$nodim->LAC</td>
                              <td>$nodim->CI</td>
                              <td>$nodim->NEW_LAC</td>
                              <td>$nodim->NEW_CI</td>
                              <td>$nodim->SITE_ID</td>
                              <td>$nodim->NE_ID</td>
                              <td>$nodim->BAND</td>
                              <td>$nodim->DESCRIPTION</td>
                              <td>$nodim->SWAP</td>
                              <td>$nodim->DESCRIPTION_OF_CHANGE</td>
                              <td>$nodim->REASON_FOR_CHANGE</td>
                              <td>$nodim->RESPONSIBLE_APPROVAL</td>
                              <td>$nodim->DETAILED_LOCATION</td>
                              <td>$nodim->NE_ID_PRIMARY</td>
                              <td>$nodim->NE_NAME_PRIMARY</td>
                              <td>$nodim->PROGRAM</td>
                              <td>$nodim->SOW</td>
                              <td>$nodim->SITE_NAME</td>
                              <td>$nodim->BSC_NAME</td>
                              <td>$nodim->BSC_ID</td>
                              <td>$nodim->MCC</td>
                              <td>$nodim->MNC</td>
                              <td>$nodim->EXECUTOR_PIC_COMPANY</td>
                              <td>$nodim->EXECUTOR_PIC_NAME</td>
                              <td>$nodim->EXECUTOR_PIC_PHONE</td>
                              <td>$nodim->EXECUTOR_PIC_EMAIL</td>
                              <td>$nodim->NETWORLELEMENT_NAME</td>
                              <td>$nodim->USER_ACCESS_LOGIN</td>
                              <td>$nodim->NE_TYPE</td>
                              <td>$nodim->AREA_NE</td>
                              <td>$nodim->ADDITIONAL_INFORMATION</td>
                              <td>$nodim->ACCESS_LEVEL</td>
                              <td>$nodim->NETWORK_TYPE</td>
                              <td>$nodim->ROLEKEYS</td>
                              <td>$nodim->AFFECTED_SERVICE_DESCRIPTION</td>
                              <td>$nodim->SEVICE_OUTAGE_DURATION_MIN</td>
                              <td>$nodim->BTS_NAME</td>
                              <td>$nodim->FREQUENCY</td>
                              <td>$nodim->AFFECTED_SERVICE</td>
                              <td>$nodim->PROVINSI</td>
                              <td>$nodim->KELURAHAN</td>
                              <td>$nodim->KECAMATAN</td>
                              <td>$nodim->CLUSTER</td>
                              <td>$nodim->CITY</td>
                            </tr>
                          ";
                        }else{
                          echo "
                            <tr>
                              <td>
                                <center>
                                <center><i class='fa fa-check-square' style='color: green;' data-toggle='tooltip' data-placement='top' title='Nomor Nodin Telah Diberikan'></i></center>
                                </center>
                              </td>
                              <td>$nodim->NODIN_ID</td>
                              <td>$nodim->CELL_NAME</td>
                              <td>$nodim->VENDOR</td>
                              <td>$nodim->REGIONAL</td>
                              <td>$nodim->TAC</td>
                              <td>$nodim->CI</td>
                              <td>$nodim->NEW_TAC</td>
                              <td>$nodim->NEW_CI</td>
                              <td>$nodim->SITE_ID</td>
                              <td>$nodim->NE_ID</td>
                              <td>$nodim->BAND</td>
                              <td>$nodim->DESCRIPTION</td>
                              <td>$nodim->SWAP</td>
                              <td>$nodim->DESCRIPTION_OF_CHANGE</td>
                              <td>$nodim->REASON_FOR_CHANGE</td>
                              <td>$nodim->RESPONSIBLE_APPROVAL</td>
                              <td>$nodim->DETAILED_LOCATION</td>
                              <td>$nodim->NE_ID_PRIMARY</td>
                              <td>$nodim->NE_NAME_PRIMARY</td>
                              <td>$nodim->PROGRAM</td>
                              <td>$nodim->SOW</td>
                              <td>$nodim->SITE_NAME</td>
                              <td>$nodim->BSC_NAME</td>
                              <td>$nodim->BSC_ID</td>
                              <td>$nodim->MCC</td>
                              <td>$nodim->MNC</td>
                              <td>$nodim->EXECUTOR_PIC_COMPANY</td>
                              <td>$nodim->EXECUTOR_PIC_NAME</td>
                              <td>$nodim->EXECUTOR_PIC_PHONE</td>
                              <td>$nodim->EXECUTOR_PIC_EMAIL</td>
                              <td>$nodim->NETWORLELEMENT_NAME</td>
                              <td>$nodim->USER_ACCESS_LOGIN</td>
                              <td>$nodim->NE_TYPE</td>
                              <td>$nodim->AREA_NE</td>
                              <td>$nodim->ADDITIONAL_INFORMATION</td>
                              <td>$nodim->ACCESS_LEVEL</td>
                              <td>$nodim->NETWORK_TYPE</td>
                              <td>$nodim->ROLEKEYS</td>
                              <td>$nodim->AFFECTED_SERVICE_DESCRIPTION</td>
                              <td>$nodim->SEVICE_OUTAGE_DURATION_MIN</td>
                              <td>$nodim->BTS_NAME</td>
                              <td>$nodim->FREQUENCY</td>
                              <td>$nodim->AFFECTED_SERVICE</td>
                              <td>$nodim->PROVINSI</td>
                              <td>$nodim->KELURAHAN</td>
                              <td>$nodim->KECAMATAN</td>
                              <td>$nodim->CLUSTER</td>
                              <td>$nodim->CITY</td>
                            </tr>
                          ";
                        }
                      }elseif($nodim->NEW_CI == "" AND $nodim->NEW_LAC == "") {
                        if($nodim->BAND != "4G") {
                          echo "
                            <tr>
                              <td>
                                <center>
                                <center><i class='fa fa-close' style='color: red;' data-toggle='tooltip' data-placement='top' title='Lengkapi Data Terlebih Dahulu'></i></center>
                                </center>
                              </td>
                              <td>Haven't Nodin ID</td>
                              <td>$nodim->CELL_NAME</td>
                              <td>$nodim->VENDOR</td>
                              <td>$nodim->REGIONAL</td>
                              <td>$nodim->LAC</td>
                              <td>$nodim->CI</td>
                              <td>$nodim->NEW_LAC</td>
                              <td>$nodim->NEW_CI</td>
                              <td>$nodim->SITE_ID</td>
                              <td>$nodim->NE_ID</td>
                              <td>$nodim->BAND</td>
                              <td>$nodim->DESCRIPTION</td>
                              <td>$nodim->SWAP</td>
                              <td>$nodim->DESCRIPTION_OF_CHANGE</td>
                              <td>$nodim->REASON_FOR_CHANGE</td>
                              <td>$nodim->RESPONSIBLE_APPROVAL</td>
                              <td>$nodim->DETAILED_LOCATION</td>
                              <td>$nodim->NE_ID_PRIMARY</td>
                              <td>$nodim->NE_NAME_PRIMARY</td>
                              <td>$nodim->PROGRAM</td>
                              <td>$nodim->SOW</td>
                              <td>$nodim->SITE_NAME</td>
                              <td>$nodim->BSC_NAME</td>
                              <td>$nodim->BSC_ID</td>
                              <td>$nodim->MCC</td>
                              <td>$nodim->MNC</td>
                              <td>$nodim->EXECUTOR_PIC_COMPANY</td>
                              <td>$nodim->EXECUTOR_PIC_NAME</td>
                              <td>$nodim->EXECUTOR_PIC_PHONE</td>
                              <td>$nodim->EXECUTOR_PIC_EMAIL</td>
                              <td>$nodim->NETWORLELEMENT_NAME</td>
                              <td>$nodim->USER_ACCESS_LOGIN</td>
                              <td>$nodim->NE_TYPE</td>
                              <td>$nodim->AREA_NE</td>
                              <td>$nodim->ADDITIONAL_INFORMATION</td>
                              <td>$nodim->ACCESS_LEVEL</td>
                              <td>$nodim->NETWORK_TYPE</td>
                              <td>$nodim->ROLEKEYS</td>
                              <td>$nodim->AFFECTED_SERVICE_DESCRIPTION</td>
                              <td>$nodim->SEVICE_OUTAGE_DURATION_MIN</td>
                              <td>$nodim->BTS_NAME</td>
                              <td>$nodim->FREQUENCY</td>
                              <td>$nodim->AFFECTED_SERVICE</td>
                              <td>$nodim->PROVINSI</td>
                              <td>$nodim->KELURAHAN</td>
                              <td>$nodim->KECAMATAN</td>
                              <td>$nodim->CLUSTER</td>
                              <td>$nodim->CITY</td>
                            </tr>
                          ";
                        }else{
                          echo "
                            <tr>
                              <td>
                                <center>
                                <center><i class='fa fa-close' style='color: red;' data-toggle='tooltip' data-placement='top' title='Lengkapi Data Terlebih Dahulu'></i></center>
                                </center>
                              </td>
                              <td>Haven't Nodin ID</td>
                              <td>$nodim->CELL_NAME</td>
                              <td>$nodim->VENDOR</td>
                              <td>$nodim->REGIONAL</td>
                              <td>$nodim->TAC</td>
                              <td>$nodim->CI</td>
                              <td>$nodim->NEW_TAC</td>
                              <td>$nodim->NEW_CI</td>
                              <td>$nodim->SITE_ID</td>
                              <td>$nodim->NE_ID</td>
                              <td>$nodim->BAND</td>
                              <td>$nodim->DESCRIPTION</td>
                              <td>$nodim->SWAP</td>
                              <td>$nodim->DESCRIPTION_OF_CHANGE</td>
                              <td>$nodim->REASON_FOR_CHANGE</td>
                              <td>$nodim->RESPONSIBLE_APPROVAL</td>
                              <td>$nodim->DETAILED_LOCATION</td>
                              <td>$nodim->NE_ID_PRIMARY</td>
                              <td>$nodim->NE_NAME_PRIMARY</td>
                              <td>$nodim->PROGRAM</td>
                              <td>$nodim->SOW</td>
                              <td>$nodim->SITE_NAME</td>
                              <td>$nodim->BSC_NAME</td>
                              <td>$nodim->BSC_ID</td>
                              <td>$nodim->MCC</td>
                              <td>$nodim->MNC</td>
                              <td>$nodim->EXECUTOR_PIC_COMPANY</td>
                              <td>$nodim->EXECUTOR_PIC_NAME</td>
                              <td>$nodim->EXECUTOR_PIC_PHONE</td>
                              <td>$nodim->EXECUTOR_PIC_EMAIL</td>
                              <td>$nodim->NETWORLELEMENT_NAME</td>
                              <td>$nodim->USER_ACCESS_LOGIN</td>
                              <td>$nodim->NE_TYPE</td>
                              <td>$nodim->AREA_NE</td>
                              <td>$nodim->ADDITIONAL_INFORMATION</td>
                              <td>$nodim->ACCESS_LEVEL</td>
                              <td>$nodim->NETWORK_TYPE</td>
                              <td>$nodim->ROLEKEYS</td>
                              <td>$nodim->AFFECTED_SERVICE_DESCRIPTION</td>
                              <td>$nodim->SEVICE_OUTAGE_DURATION_MIN</td>
                              <td>$nodim->BTS_NAME</td>
                              <td>$nodim->FREQUENCY</td>
                              <td>$nodim->AFFECTED_SERVICE</td>
                              <td>$nodim->PROVINSI</td>
                              <td>$nodim->KELURAHAN</td>
                              <td>$nodim->KECAMATAN</td>
                              <td>$nodim->CLUSTER</td>
                              <td>$nodim->CITY</td>
                            </tr>
                          ";
                        }
                      }else {
                        if($nodim->BAND != "4G") {
                          echo "
                            <tr>
                              <td>
                                <center>
                                <input type='checkbox' name='checkbox[".$i."]' value='".$nodim->CELL_NAME."'>
                                </center>
                              </td>
                              <td>Haven't Nodin ID</td>
                              <td>$nodim->CELL_NAME</td>
                              <td>$nodim->VENDOR</td>
                              <td>$nodim->REGIONAL</td>
                              <td>$nodim->LAC</td>
                              <td>$nodim->CI</td>
                              <td>$nodim->NEW_LAC</td>
                              <td>$nodim->NEW_CI</td>
                              <td>$nodim->SITE_ID</td>
                              <td>$nodim->NE_ID</td>
                              <td>$nodim->BAND</td>
                              <td>$nodim->DESCRIPTION</td>
                              <td>$nodim->SWAP</td>
                              <td>$nodim->DESCRIPTION_OF_CHANGE</td>
                              <td>$nodim->REASON_FOR_CHANGE</td>
                              <td>$nodim->RESPONSIBLE_APPROVAL</td>
                              <td>$nodim->DETAILED_LOCATION</td>
                              <td>$nodim->NE_ID_PRIMARY</td>
                              <td>$nodim->NE_NAME_PRIMARY</td>
                              <td>$nodim->PROGRAM</td>
                              <td>$nodim->SOW</td>
                              <td>$nodim->SITE_NAME</td>
                              <td>$nodim->BSC_NAME</td>
                              <td>$nodim->BSC_ID</td>
                              <td>$nodim->MCC</td>
                              <td>$nodim->MNC</td>
                              <td>$nodim->EXECUTOR_PIC_COMPANY</td>
                              <td>$nodim->EXECUTOR_PIC_NAME</td>
                              <td>$nodim->EXECUTOR_PIC_PHONE</td>
                              <td>$nodim->EXECUTOR_PIC_EMAIL</td>
                              <td>$nodim->NETWORLELEMENT_NAME</td>
                              <td>$nodim->USER_ACCESS_LOGIN</td>
                              <td>$nodim->NE_TYPE</td>
                              <td>$nodim->AREA_NE</td>
                              <td>$nodim->ADDITIONAL_INFORMATION</td>
                              <td>$nodim->ACCESS_LEVEL</td>
                              <td>$nodim->NETWORK_TYPE</td>
                              <td>$nodim->ROLEKEYS</td>
                              <td>$nodim->AFFECTED_SERVICE_DESCRIPTION</td>
                              <td>$nodim->SEVICE_OUTAGE_DURATION_MIN</td>
                              <td>$nodim->BTS_NAME</td>
                              <td>$nodim->FREQUENCY</td>
                              <td>$nodim->AFFECTED_SERVICE</td>
                              <td>$nodim->PROVINSI</td>
                              <td>$nodim->KELURAHAN</td>
                              <td>$nodim->KECAMATAN</td>
                              <td>$nodim->CLUSTER</td>
                              <td>$nodim->CITY</td>
                            </tr>
                          ";
                        }else{
                          echo "
                            <tr>
                              <td>
                                <center>
                                <input type='checkbox' name='checkbox[".$i."]' value='".$nodim->CELL_NAME."'>
                                </center>
                              </td>
                              <td>Haven't Nodin ID</td>
                              <td>$nodim->CELL_NAME</td>
                              <td>$nodim->VENDOR</td>
                              <td>$nodim->REGIONAL</td>
                              <td>$nodim->TAC</td>
                              <td>$nodim->CI</td>
                              <td>$nodim->NEW_TAC</td>
                              <td>$nodim->NEW_CI</td>
                              <td>$nodim->SITE_ID</td>
                              <td>$nodim->NE_ID</td>
                              <td>$nodim->BAND</td>
                              <td>$nodim->DESCRIPTION</td>
                              <td>$nodim->SWAP</td>
                              <td>$nodim->DESCRIPTION_OF_CHANGE</td>
                              <td>$nodim->REASON_FOR_CHANGE</td>
                              <td>$nodim->RESPONSIBLE_APPROVAL</td>
                              <td>$nodim->DETAILED_LOCATION</td>
                              <td>$nodim->NE_ID_PRIMARY</td>
                              <td>$nodim->NE_NAME_PRIMARY</td>
                              <td>$nodim->PROGRAM</td>
                              <td>$nodim->SOW</td>
                              <td>$nodim->SITE_NAME</td>
                              <td>$nodim->BSC_NAME</td>
                              <td>$nodim->BSC_ID</td>
                              <td>$nodim->MCC</td>
                              <td>$nodim->MNC</td>
                              <td>$nodim->EXECUTOR_PIC_COMPANY</td>
                              <td>$nodim->EXECUTOR_PIC_NAME</td>
                              <td>$nodim->EXECUTOR_PIC_PHONE</td>
                              <td>$nodim->EXECUTOR_PIC_EMAIL</td>
                              <td>$nodim->NETWORLELEMENT_NAME</td>
                              <td>$nodim->USER_ACCESS_LOGIN</td>
                              <td>$nodim->NE_TYPE</td>
                              <td>$nodim->AREA_NE</td>
                              <td>$nodim->ADDITIONAL_INFORMATION</td>
                              <td>$nodim->ACCESS_LEVEL</td>
                              <td>$nodim->NETWORK_TYPE</td>
                              <td>$nodim->ROLEKEYS</td>
                              <td>$nodim->AFFECTED_SERVICE_DESCRIPTION</td>
                              <td>$nodim->SEVICE_OUTAGE_DURATION_MIN</td>
                              <td>$nodim->BTS_NAME</td>
                              <td>$nodim->FREQUENCY</td>
                              <td>$nodim->AFFECTED_SERVICE</td>
                              <td>$nodim->PROVINSI</td>
                              <td>$nodim->KELURAHAN</td>
                              <td>$nodim->KECAMATAN</td>
                              <td>$nodim->CLUSTER</td>
                              <td>$nodim->CITY</td>
                            </tr>
                          ";
                        }
                      }
                    ?>
                    <input type="hidden" name="CI[<?=$i;?>]" value="<?= $nodim->CI; ?>">
                    <input type="hidden" name="LAC[<?=$i;?>]" value="<?= $nodim->LAC; ?>">
                    <input type="hidden" name="VENDOR[<?=$i;?>]" value="<?= $nodim->VENDOR; ?>">
                    <input type="hidden" name="BAND[<?=$i;?>]" value="<?= $nodim->BAND; ?>">
                    <input type="hidden" name="SITE_ID[<?=$i;?>]" value="<?= $nodim->SITE_ID; ?>">
                    <input type="hidden" name="CELL_NAME[<?=$i;?>]" value="<?= $nodim->CELL_NAME; ?>">
                    <input type="hidden" name="NEW_LAC[<?=$i;?>]" value="<?= $nodim->NEW_LAC; ?>">
                    <input type="hidden" name="NEW_TAC[<?=$i;?>]" value="<?= $nodim->NEW_TAC; ?>">
                    <input type="hidden" name="NEW_CI[<?=$i;?>]" value="<?= $nodim->NEW_CI; ?>">
                    <input type="hidden" name="NE_ID[<?=$i;?>]" value="<?= $nodim->NE_ID; ?>">
                    <input type="hidden" name="DESCRIPTION_OF_CHANGE[<?=$i;?>]" value"<?= $nodim->DESCRIPTION_OF_CHANGE;?>">
                    <input type="hidden" name="REASON_FOR_CHANGE[<?=$i;?>]" value="<?=$nodim->REASON_FOR_CHANGE?>">
                    <input type="hidden" name="RESPONSIBLE_APPROVAL[<?=$i;?>]" value="<?=$nodim->RESPONSIBLE_APPROVAL?>">
                    <input type="hidden" name="DETAILED_LOCATION[<?=$i;?>]" value="<?=$nodim->DETAILED_LOCATION?>">
                    <input type="hidden" name="IMPACT_TO_NE[<?=$i;?>]" value="<?=$nodim->IMPACT_TO_NE?>">
                    <input type="hidden" name="NE_ID_PRIMARY[<?=$i;?>]" value="<?=$nodim->NE_ID_PRIMARY?>">
                    <input type="hidden" name="NE_NAME_PRIMARY[<?=$i;?>]" value="<?=$nodim->NE_NAME_PRIMARY?>">
                    <input type="hidden" name="PO_NUMBER[<?=$i;?>]" value="<?=$nodim->PO_NUMBER?>">
                    <input type="hidden" name="PROGRAM[<?=$i;?>]" value="<?=$nodim->PROGRAM?>">
                    <input type="hidden" name="SOW[<?=$i;?>]" value="<?=$nodim->SOW?>">
                    <input type="hidden" name="SITE_NAME[<?=$i;?>]" value="<?=$nodim->SITE_NAME?>">
                    <input type="hidden" name="BSC_NAME[<?=$i;?>]" value="<?=$nodim->BSC_NAME?>">
                    <input type="hidden" name="BSC_ID[<?=$i;?>]" value="<?=$nodim->BSC_ID?>">
                    <input type="hidden" name="NETWORK_TYPE[<?=$i;?>]" value="<?=$nodim->NETWORK_TYPE?>">
                    <input type="hidden" name="LONGITUDE[<?=$i;?>]" value="<?=$nodim->LONGITUDE?>">
                    <input type="hidden" name="LATITUDE[<?=$i;?>]" value="<?=$nodim->LATITUDE?>">
                    <input type="hidden" name="BSC_NAME[<?=$i;?>]" value="<?=$nodim->BSC_NAME?>">
                    <input type="hidden" name="MCC[<?=$i;?>]" value="<?=$nodim->MCC?>">
                    <input type="hidden" name="MNC[<?=$i;?>]" value="<?=$nodim->MNC?>">
                    <input type="hidden" name="EXECUTOR_INSTANCES[<?=$i;?>]" value="<?=$nodim->EXECUTOR_INSTANCES?>">
                    <input type="hidden" name="EXECUTOR_PIC_COMPANY[<?=$i;?>]" value="<?=$nodim->EXECUTOR_PIC_COMPANY?>">
                    <input type="hidden" name="EXECUTOR_PIC_NAME[<?=$i;?>]" value="<?=$nodim->EXECUTOR_PIC_NAME?>">
                    <input type="hidden" name="EXECUTOR_PIC_PHONE[<?=$i;?>]" value="<?=$nodim->EXECUTOR_PIC_PHONE?>">
                    <input type="hidden" name="EXECUTOR_PIC_EMAIL[<?=$i;?>]" value="<?=$nodim->EXECUTOR_PIC_EMAIL?>">
                    <input type="hidden" name="NETWORLELEMENT_NAME[<?=$i;?>]" value="<?=$nodim->NETWORLELEMENT_NAME?>">
                    <input type="hidden" name="USER_ACCESS_LOGIN[<?=$i;?>]" value="<?=$nodim->USER_ACCESS_LOGIN?>">
                    <input type="hidden" name="NE_TYPE[<?=$i;?>]" value="<?=$nodim->NE_TYPE?>">
                    <input type="hidden" name="REGION_NE[<?=$i;?>]" value="<?=$nodim->REGION_NE?>">
                    <input type="hidden" name="AREA_NE[<?=$i;?>]" value="<?=$nodim->AREA_NE?>">
                    <input type="hidden" name="ADDITIONAL_INFORMATION[<?=$i;?>]" value="<?=$nodim->ADDITIONAL_INFORMATION?>">
                    <input type="hidden" name="ACCESS_LEVEL[<?=$i;?>]" value="<?=$nodim->ACCESS_LEVEL?>">
                    <input type="hidden" name="NETWORK_TYPE[<?=$i;?>]" value="<?=$nodim->NETWORK_TYPE?>">
                    <input type="hidden" name="ROLEKEYS[<?=$i;?>]" value="<?=$nodim->ROLEKEYS?>">
                    <input type="hidden" name="PO_NUMBER[<?=$i;?>]" value="<?=$nodim->PO_NUMBER?>">
                    <input type="hidden" name="PROGRAM[<?=$i;?>]" value="<?=$nodim->PROGRAM?>">
                    <input type="hidden" name="AFFECTED_SERVICE_DESCRIPTION[<?=$i;?>]" value="<?=$nodim->AFFECTED_SERVICE_DESCRIPTION?>">
                    <input type="hidden" name="SEVICE_OUTAGE_DURATION_MIN[<?=$i;?>]" value="<?=$nodim->SEVICE_OUTAGE_DURATION_MIN?>">
                    <input type="hidden" name="BTS_NAME[<?=$i;?>]" value="<?=$nodim->BTS_NAME?>">
                    <input type="hidden" name="AFFECTED_SERVICE[<?=$i?>]" value="<?=$nodim->AFFECTED_SERVICE?>">
                    <input type="hidden" name="NE_IMPACT_DESCRIPTION[<?=$i?>]" value="<?=$nodim->NE_IMPACT_DESCRIPTION?>">
                    <input type="hidden" name="NE_NAME_IMPACTED_LIST[<?=$i?>]" value="<?=$nodim->NE_NAME_IMPACTED_LIST?>">
                    <input type="hidden" name="NE_OUTAGE_DURATION_MIN[<?=$i?>]" value="<?=$nodim->NE_OUTAGE_DURATION_MIN?>">
                    <input type="hidden" name="SERVICE_OUTAGE_DESCRIPTION_MIN[<?=$i?>]" value="<?=$nodim->SEVICE_OUTAGE_DURATION_MIN?>">
                    <input type="hidden" name="PROVINSI[<?=$i?>]" value="<?=$nodim->PROVINSI?>">
                    <input type="hidden" name="KELURAHAN[<?=$i?>]" value="<?=$nodim->KELURAHAN?>">
                    <input type="hidden" name="KECAMATAN[<?=$i?>]" value="<?=$nodim->KECAMATAN?>">
                    <input type="hidden" name="CLUSTER[<?=$i?>]" value="<?=$nodim->CLUSTER?>">
                    <input type="hidden" name="CITY[<?=$i?>]" value="<?=$nodim->CITY?>">
                    <?php
                      $i++;
                    }
                    ?>
                  </tbody>
                </table>
                <div class="col-md-12">
                  <div id="pagination" class="scrollmenu">
                    <ul class="tsc_pagination">

                    <!-- Show pagination links -->
                    <?php foreach ($links as $link) {
                    echo "<li> ". $link." </li>";
                    } ?>
                    </ul>
                  </div>
                </div>
                <!-- /.table -->
              </div>

              <div class="card-footer" style="margin-top:50px;">
                <div class="row" style="width:100%">
                  <div class="float-right" style="width:70%">
                    <div class="row" style="margin-left: 8px; margin-top: -40px;">
                      <?php
                        echo form_submit('submit','Submit',array('class' => 'btn btn-sm btn-primary sendButton','id' => 'sendButton'));
                      ?>
                      <select class="form-control" name="setting" style="width:auto; margin:5px;">
                        <option name="setting" value="significant">Significant</option>
                        <option name="setting" value="standard">Standard (Retail)</option>
                      </select>
                      <input name="swap" class="form-control" type="text" id="message" placeholder="Swap to ..." style="width:auto; margin:5px;">
                      <input name="reason" class="form-control" type="text" id="message" placeholder="Reason ..." style="width:auto; margin:5px;">
                      <input type="file" name="file_mop" accept="text/plain" style="width:auto; margin:5px; margin-left:65px;">
                      <input type="file" name="file_timeline" accept="application/msexcel" style="width:auto; margin:5px;">
                    </div>
                  </div>
                  <?php echo form_close(); ?>
                  <div class="float-left" style="width:30%;">
                    <?php echo  form_open_multipart('NodinController/export_data_excel');?>
                    <div class="row" style="margin-left: 30px; margin-top: -40px;">
                      <input name="filter_export" class="form-control" type="text" id="filter_export" placeholder="Export to excel base on NE ID" style="width:auto; margin:5px;">
                      <?php
                        echo form_submit('submit','Export',array('class' => 'btn btn-sm btn-primary exportButton','id' => 'exportButton'));
                      ?>
                    </div>
                    <?php echo form_close(); ?>
                  </div>
                </div>
              </div>
        </div>
        </div>
      </div>
    <!-- /.content -->
    </div>
  <!-- /.content-wrapper -->
    <footer class="main-footer sticky-bottom" style="margin-left: 250px; background: white; color: black;">
    <strong style="font-size: 12px">Copyright &copy; 2018 <a style="color:white;">Telkomsel</a>.</strong>
    <div class="float-right d-none d-sm-inline-block">
        </div>
    </footer>
    </div><!-- /container -->
    <script src="<?php echo base_url('asset/plugins/google/js/classie.js');?>"></script>
    <script src="<?php echo base_url('asset/plugins/google/js/gnmenu.js');?>"></script>
    <!-- jQuery -->
    <script type="text/javascript" src="<?php echo base_url('/asset/plugins/jquery/jquery.min.js'); ?>"></script>
    <script src="<?php echo base_url('asset/plugins/jQueryUI/jquery-ui.min.js'); ?>"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <!-- Bootstrap 4 -->
    <script type="text/javascript" src="<?php echo base_url('asset/plugins/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <!-- Slimscroll -->
    <script type="text/javascript" src="<?php echo base_url('asset/plugins/slimScroll/jquery.slimscroll.min.js'); ?>"></script>
    <!-- FastClick -->
    <script type="text/javascript" src="<?php echo base_url('asset/plugins/fastclick/fastclick.js'); ?>"></script>
    <!-- AdminLTE App -->
    <script type="text/javascript" src="<?php echo base_url('asset/dist/js/adminlte.min.js'); ?>"></script>
    <!-- iCheck -->
    <script type="text/javascript" src="<?php echo base_url('asset/plugins/iCheck/icheck.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/plugins/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('asset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js');?>"></script>
    <script type="text/javascript">
      $(document).ready(function(){
          $('.sendButton').attr('disabled',true);
          $('#message').keyup(function(){
              if($(this).val().length !=0)
                  $('.sendButton').attr('disabled', false);
              else
                  $('.sendButton').attr('disabled',true);
          })
      });
    </script>
    <script type="text/javascript">
      $('#filter_export').autocomplete({
        source:function(request, response){
          $.ajax({
            url:"<?php echo base_url('index.php/NodinController/auto_neid'); ?>",
            method:'get',
            data:{
              term: request.term
            },
            success:function(dataSuccess){
              response(dataSuccess);
            }
          });
        },
        minLength:0,
        focus:function(event, ui){
          $('#filter_export').val(ui.item.value);
          return false;
        },
        select:function(event, ui){
          $('#filter_export').val(ui.item.value);
        },
        change:function(event, ui){
          if (ui.item===null) {
            $('#filter_export').val('');
          }
        }
      }).bind('focus', function(){
        $(this).autocomplete("search");
      });
    </script>
    <script>
      new gnMenu( document.getElementById( 'gn-menu' ) );
    </script>
    <script>
    (function ($) {
    //Enable iCheck plugin for checkboxes
    //iCheck for checkbox and radio inputs
    $('.mailbox-messages input[type="checkbox"]').iCheck({
      checkboxClass: 'icheckbox_flat-blue',
      radioClass   : 'iradio_flat-blue'
    })

    //Enable check and uncheck all functionality
    $('.checkbox-toggle').click(function () {
      var clicks = $(this).data('clicks')
      if (clicks) {
        //Uncheck all checkboxes
        $('.mailbox-messages input[type=\'checkbox\']').iCheck('uncheck')
        $('.fa', this).removeClass('fa-check-square').addClass('fa-square')
      } else {
        //Check all checkboxes
        $('.mailbox-messages input[type=\'checkbox\']').iCheck('check')
        $('.fa', this).removeClass('fa-square').addClass('fa-check-square')
      }
      $(this).data('clicks', !clicks)
    })

    //Handle starring for glyphicon and font awesome
    $('.mailbox-star').click(function (e) {
      e.preventDefault()
      //detect type
      var $this = $(this).find('a > i')
      var glyph = $this.hasClass('glyphicon')
      var fa    = $this.hasClass('fa')

      //Switch states
      if (glyph) {
        $this.toggleClass('glyphicon-star')
        $this.toggleClass('glyphicon-star-empty')
      }

      if (fa) {
        $this.toggleClass('fa-star')
        $this.toggleClass('fa-star-o')
      }
    })
  })(jQuery)
</script>
  </body>
</html>

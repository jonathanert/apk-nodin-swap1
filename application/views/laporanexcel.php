 <?php

 header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

 header("Content-Disposition: attachment; filename=test.xlsx");

 header("Pragma: no-cache");

 header("Expires: 0");

 ?>

 <table border="1" width="100%">

      <thead>
       <tr>
          <th>No</th>
          <th>Site ID</th>
          <th>NE ID</th>
          <!-- <th>Sector ID</th> -->
          <!-- <th>Site Name</th> -->
          <!-- <th>OSS Name</th> -->
          <th>Kabupaten</th>
          <th>LAC</th>
          <th>Cell Name</th>
          <th>CI/SAC</th>
          <th>Scrambling Code</th>
          <th>RNC</th>
          <th>RNC ID</th>
          <th>RNC SCP</th>
          <th>RAC</th>
          <th>URA ID</th>
          <th>MSCS Name</th>
          <th>MSCS SPC</th>
          <th>MGW Name</th>
          <th>MGW SPC</th>
          <th>LOCNO</th>
          <th>POC PSTN</th>
          <th>Time Zone</th>
          <th>SOW</th>
          <th>LONG</th>
          <th>LAT</th>
       </tr>
      </thead>
      <tbody>
       <?php $i=1; foreach($datanodin as $nodin) { ?>
       <tr>
          <td><?php echo $i;?></td>
          <td><?php echo $nodin->NODIN_ID; ?></td>
          <td><?php echo $nodin->SITE_ID; ?></td>
          <td><?php echo $nodin->NE_ID; ?></td>
          <td><?php echo $nodin->kabupaten; ?></td>
          <td><?php echo $nodin->lac; ?></td>
          <td><?php echo $nodin->cell_name; ?></td>
          <td><?php echo $nodin->sac; ?></td>
          <td><?php echo $nodin->scrambling_code; ?></td>
          <td><?php echo $nodin->rnc; ?></td>
          <td><?php echo $nodin->rnc_id; ?></td>
          <td><?php echo $nodin->rnc_scp; ?></td>
          <td><?php echo $nodin->rac; ?></td>
          <td><?php echo $nodin->ura_id; ?></td>
          <td><?php echo $nodin->mscs_name; ?></td>
          <td><?php echo $nodin->mscs_spc; ?></td>
          <td><?php echo $nodin->mgw_name; ?></td>
          <td><?php echo $nodin->mgw_spc; ?></td>
          <td><?php echo $nodin->locno; ?></td>
          <td><?php echo $nodin->poc_pstn; ?></td>
          <td><?php echo $nodin->time_zone; ?></td>
          <td><?php echo $nodin->sow; ?></td>
          <td><?php echo $nodin->longtitude; ?></td>
          <td><?php echo $nodin->latitude; ?></td>
       </tr>
       <?php $i++; } ?>

      </tbody>

 </table>
